<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStoreToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('store', 32)->nullable();
            $table->string('purchase_token')->nullable();
            $table->string('order_id')->nullable();
            $table->string('original_order_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('store');
            $table->dropColumn('purchase_token');

            $table->dropColumn('original_order_id');
            $table->dropColumn('order_id');
        });
    }
}
