<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaxFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fax_files', function (Blueprint $table) {
            $table->id();
            $table->integer('fax_id');
            $table->integer('page');
            $table->string('file_name');
            $table->string('file_path');
            $table->string('fax_sid', 64)->nullable();
            $table->string('status', 16)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fax_files');
    }
}
