<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->boolean('is_cancelled')->default(false);
            $table->boolean('is_renewed')->default(false);
            $table->timestamp('renewed_date')->nullable()->default(null);
            $table->timestamp('cancelled_date')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('is_cancelled');
            $table->dropColumn('is_renewed');
            $table->dropColumn('renewed_date');
            $table->dropColumn('cancelled_date');
        });
    }
}
