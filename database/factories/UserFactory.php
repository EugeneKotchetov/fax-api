<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {

    return [
        'username' => 'fax-' . bin2hex(random_bytes(10)),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'os_version' => $faker->randomElement(['v7', 'v8', 'v9', 'v10', 'v11']),
        'ip' => $faker->ipv4,
        'country' => $faker->randomElement(['ru', 'us', 'pl', 'kz', 'gb']),
        'is_active' => $faker->boolean(90),
        'tariff_id' => 0,
        'paid_until' => 0,
        'payment_amount' => null,
    ];
});
