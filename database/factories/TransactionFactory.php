<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Transaction;
use App\Models\User;
use App\Models\Tariff;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {

    $tariff = Tariff::inRandomOrder()->first();

    return [
        'user_id' => User::inRandomOrder()->first(),
        'original_transaction_id' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'transaction_id' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'tariff_apple_id' => $tariff->apple_id,
        'receipt' => null,
        'purchase_date' => now(),
    ];
});
