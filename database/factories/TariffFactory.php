<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tariff;
use Faker\Generator as Faker;

$factory->define(Tariff::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'apple_id' => $faker->numerify('app_###'),
        'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'discount' => '0',
        'by_default' => true,
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100),
        'days' => $faker->randomElement($array = ['7','30','365']),
        'is_active' => $faker->boolean(90),
        'sort' => 1,
    ];
});
