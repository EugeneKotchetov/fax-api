<?php

use Illuminate\Database\Seeder;
use App\Models\Tariff;

class TariffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tariff = factory(Tariff::class, 15)
            ->create();
    }
}
