<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Faker\Generator as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        DB::table('users')->insert([
            [
                'username' => 'user1',
                'password' => Hash::make('secret'),
                'os_version' => 'os_version',
                'ip' => '10.10.10.10',
                'country' => 'ru',
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);

        $users = factory(User::class, 100)
            ->create()
            ->each(function ($user) use($faker) {
                $dateTime = $faker->dateTimeBetween($startDate = '-70 days', $endDate = 'now', $timezone = null);
                $user->created_at = $dateTime;
                $user->updated_at = $dateTime;
                $user->save();
            });
    }
}
