<?php

use Illuminate\Database\Seeder;
use App\Models\Transaction;
use Faker\Generator as Faker;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $transaction = factory(Transaction::class, 100)
            ->create()
            ->each(function ($transaction) use($faker) {
                $dateTime = $faker->dateTimeBetween($startDate = '-70 days', $endDate = 'now', $timezone = null);
                $transaction->purchase_date = $dateTime;
                $transaction->created_at = $dateTime;
                $transaction->updated_at = $dateTime;
                $transaction->save();
            });
    }
}
