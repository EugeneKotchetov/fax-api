<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::group(['middleware' => 'auth.api'], function () {
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
    });
});

Route::group(['middleware' => ['auth.api', 'active_user']], function() {

    Route::get('tariffs', ['uses' => 'TariffController@list']);

    Route::post('payment', ['uses' => 'PaymentController@index']);

    Route::post('fax/create', ['uses' => 'FaxController@create']);
    Route::post('fax/{fax}/upload', ['uses' => 'FaxController@addFile']);
    Route::post('fax/{fax}/send', ['uses' => 'FaxController@send']);
    Route::get('fax/history', ['uses' => 'FaxController@history']);
    Route::delete('fax/clean', ['uses' => 'FaxController@cleanHistory']);

    Route::post('feedback', ['uses' => 'FeedbackController@store']);

    Route::post('token', ['uses' => 'PushController@storeToken']);
});

Route::post('payment/prolongation', ['uses' => 'PaymentController@prolongation']);
