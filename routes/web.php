<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/swagger-doc', 'SwaggerController@index')->name('swagger');
Route::get('/swagger-redoc', 'SwaggerController@redoc')->name('swagger.redoc');
Route::get('/swagger-doc/app.yaml', 'SwaggerController@getYaml')->name('swagger.yaml');

Route::group(['middleware' => 'auth:admins'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

	Route::resource('admins', 'AdminController', ['except' => ['show']]);
    Route::put('admins/{admin}/password', ['as' => 'admins.password', 'uses' => 'AdminController@password']);

	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    Route::get('users', 'UserController@index')->name('users.index');
    Route::post('users/{user}/toggle-active', 'UserController@toggleActive')->name('users.toggle-active');

    Route::resource('tariffs', 'TariffController', ['except' => ['show']]);

    Route::get('transactions', 'TransactionController@index')->name('transactions.index');
    Route::get('transactions/csv', 'TransactionController@toCsv')->name('transactions.csv');

    Route::get('statistics', 'StatisticController@index')->name('statistics.index');
    Route::post('statistics/fetch', 'StatisticController@fetch')->name('statistics.fetch');
    Route::get('statistics/csv', 'StatisticController@toCsv')->name('statistics.csv');

    Route::get('rebills', 'RebillController@index')->name('rebills.index');

    Route::get('feedback', 'FeedbackController@index')->name('feedbacks.index');

    Route::get('cohort-analysis', 'CohortController@index')->name('cohort.index');
    Route::post('cohort-analysis', 'CohortController@index')->name('cohort.index');
});

