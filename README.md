# Admin + API by Laravel + Argon template

## Ссылки

- [Laravel - Simple, fast routing engine](https://laravel.com/docs/routing).
- [Argon template](https://www.creative-tim.com/product/argon-dashboard-laravel).

## Fax
Для отправка факсов используется twilio. 

## Start
```
php artisan jwt:secret
npm install && npm run dev
php artisan migrate --seed
php artisan storage:link
```