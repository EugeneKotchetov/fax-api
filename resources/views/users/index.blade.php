@extends('layouts.app', ['title' => __('Users management')])

@section('content')
    @include('layouts.headers.partials.header', [
       'title' => 'Users list',
       'description' => __('This is list of users page.'),
       'class' => 'col-lg-12'
   ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-md-4">
                                <h3 class="mb-0">Users, (total {{ $users->total() }})</h3>
                            </div>
                            <div class="ml-auto col-md-6">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 60%">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fa fa-caret-down"></i>
                                    </div>
                                    <div class="ml-3 custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkShowPaid">
                                        <label class="custom-control-label" for="checkShowPaid">Show Paying Users</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Username</th>
                                <th scope="col">Is active</th>
                                <th scope="col">IP address</th>
                                <th scope="col">Country</th>
                                <th scope="col">Creation Date</th>
                                <th scope="col">Is paid</th>
                                <th scope="col">Tariffs</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->getShortUsername() }}</td>
                                    <td>
                                        @if ($user->is_active)
                                            <span class="badge badge-pill badge-success">Active</span>
                                        @else
                                            <span class="badge badge-pill badge-danger">Block</span>
                                        @endif
                                    </td>
                                    <td>{{ $user->ip }}</td>
                                    <td>{{ $user->country }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>
                                        @if ($user->is_paid)
                                            <span class="badge badge-pill badge-success">Yes</span>
                                        @else
                                            <span class="badge badge-pill badge-danger">No</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $user->tariff_list }}
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <form method="post" action="{{ route('users.toggle-active', $user->id) }}">
                                                    @csrf
                                                    <button type="submit" class="dropdown-item btn btn-link" style="font-weight: normal;">
                                                        @if ($user->is_active)
                                                            Block user
                                                        @else
                                                            Activate user
                                                        @endif
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        {{ $users->withQueryString()->links('layouts.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(function() {

            let start = moment('{!! $daterange[0] !!}');
            let end = moment('{!! $daterange[1] !!}');
            let dates = [start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD')];

            $('#checkShowPaid').prop('checked', {!! $showpaid !!});
            let showPaid = $('#checkShowPaid').is(':checked') ? true : false;

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

            $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                dates = [];
                dates.push(picker.startDate.format('YYYY-MM-DD'));
                dates.push(picker.endDate.format('YYYY-MM-DD'));
                window.location = `users?daterange=${encodeURIComponent(JSON.stringify(dates))}&showpaid=${showPaid}`;
            });

            $(document).on('click','#checkShowPaid',function(e){
                showPaid = $('#checkShowPaid').is(':checked') ? true : false;
                window.location = `users?daterange=${encodeURIComponent(JSON.stringify(dates))}&showpaid=${showPaid}`;
            });

        });
    </script>
@endpush
