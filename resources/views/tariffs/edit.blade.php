@extends('layouts.app', ['title' => __('Edit tariffs')])

@section('content')
    @include('layouts.headers.partials.header', [
       'title' => 'Edit Tariff',
       'description' => __('Edit tariff.'),
       'class' => 'col-lg-12'
   ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="mx-auto col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Update Tariff') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('tariffs.update', $tariff->id) }}" autocomplete="off">
                            @csrf
                            @method('PUT')

                            <h6 class="heading-small text-muted mb-4">{{ __('Tariff information') }}</h6>

                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-title">{{ __('Title') }}</label>
                                    <input type="text" name="title" id="input-title" class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder="{{ __('Title') }}" value="{{ old('title', $tariff->title) }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('apple_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-apple-id">{{ __('Apple ID') }}</label>
                                    <input type="text" name="apple_id" id="input-apple-id" class="form-control form-control-alternative{{ $errors->has('apple_id') ? ' is-invalid' : '' }}" placeholder="{{ __('Apple id') }}" value="{{ old('apple_id', $tariff->apple_id) }}" required>

                                    @if ($errors->has('apple_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('apple_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-description">{{ __('Description') }}</label>
                                    <input type="text" name="description" id="input-description" class="form-control form-control-alternative{{ $errors->has('description') ? ' is-invalid' : '' }}" placeholder="{{ __('Description') }}" value="{{ old('description', $tariff->description) }}" required autofocus>

                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('discount') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-discount">{{ __('Discount') }}</label>
                                    <input type="text" name="discount" id="input-discount" class="form-control form-control-alternative{{ $errors->has('discount') ? ' is-invalid' : '' }}" placeholder="{{ __('Discount') }}" value="{{ old('discount', $tariff->discount) }}" required autofocus>

                                    @if ($errors->has('discount'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('discount') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('by_default') ? ' has-danger' : '' }}">
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="by_default" value="0">
                                        <input type="checkbox" class="custom-control-input" name="by_default" id="check-by-default" value="1" {{ old('by_default',$tariff->by_default) ? 'checked' : '' }} >
                                        <label class="custom-control-label" for="check-by-default">{{ __('By default') }}</label>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-price">{{ __('Price') }}</label>
                                    <input type="text" name="price" id="input-price" class="form-control form-control-alternative{{ $errors->has('price') ? ' is-invalid' : '' }}" placeholder="{{ __('Price') }}" value="{{ old('price', $tariff->price) }}" required autofocus>

                                    @if ($errors->has('price'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('days') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-days">{{ __('Days') }}</label>
                                    <input type="text" name="days" id="input-days" class="form-control form-control-alternative{{ $errors->has('days') ? ' is-invalid' : '' }}" placeholder="{{ __('Days') }}" value="{{ old('days', $tariff->days) }}" required autofocus>

                                    @if ($errors->has('days'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('days') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('sort') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-sort">{{ __('Sort') }}</label>
                                    <input type="text" name="sort" id="input-sort" class="form-control form-control-alternative{{ $errors->has('sort') ? ' is-invalid' : '' }}" placeholder="{{ __('Sort') }}" value="{{ old('sort', $tariff->sort) }}" required autofocus>

                                    @if ($errors->has('sort'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('sort') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('group') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-group">{{ __('Group') }}</label>
                                    <input type="text" name="group" id="input-group" class="form-control form-control-alternative{{ $errors->has('group') ? ' is-invalid' : '' }}" placeholder="{{ __('Group') }}" value="{{ old('group', $tariff->group) }}" required autofocus>

                                    @if ($errors->has('group'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('group') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('is_active') ? ' has-danger' : '' }}">
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="is_active" value="0">
                                        <input type="checkbox" class="custom-control-input" name="is_active" id="check-is-active" value="1" {{ old('is_active', $tariff->is_active) ? 'checked' : '' }} >
                                        <label class="custom-control-label" for="check-is-active">{{ __('Is active') }}</label>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Update') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
