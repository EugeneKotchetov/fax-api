@extends('layouts.app', ['title' => __('Tariffs list')])

@section('content')
    @include('layouts.headers.partials.header', [
       'title' => 'Tariffs list',
       'description' => __('This is list of tariffs page.'),
       'class' => 'col-lg-12'
   ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Tariffs</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('tariffs.create') }}" class="btn btn-sm btn-primary">Add tariff</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Title</th>
                                <th scope="col">Apple_id</th>
                                <th scope="col">Description</th>
                                <th scope="col">Discount</th>
                                <!--<th scope="col">By default</th>-->
                                <th scope="col">Price</th>
                                <th scope="col">Days</th>
                                <th scope="col">Group</th>
                                <th scope="col">Is active</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tariffs as $tariff)
                                <tr class="group-{{ $tariff->group }}">
                                    <td>{{ $tariff->id }}</td>
                                    <td>{{ $tariff->title }}</td>
                                    <td>{{ $tariff->apple_id }}</td>
                                    <td>{{ $tariff->description }}</td>
                                    <td>{{ $tariff->discount }}</td>
                                    <!--<td>{{ $tariff->by_default }}</td>-->
                                    <td>{{ $tariff->price }}</td>
                                    <td>{{ $tariff->days }}</td>
                                    <td>{{ $tariff->group }}</td>
                                    <td>
                                        @if ($tariff->is_active)
                                            <span class="badge badge-pill badge-success">Active</span>
                                        @else
                                            <span class="badge badge-pill badge-danger">Block</span>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('tariffs.edit', $tariff->id) }}">Edit</a>
                                                <form method="post" action="{{ route('tariffs.destroy', $tariff->id) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn_delete dropdown-item btn btn-link" style="font-weight: normal;">{{ __('Delete') }}</button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">

                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
