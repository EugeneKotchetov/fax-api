<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-4 col-lg-6">
                    <a id="users_today" href="#">
                        <div class="card card-stats mb-4 mb-xl-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Today users</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $usersTodayTotal }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-pink text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    @if ($usersTodayTrend > 0)
                                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{ $usersTodayTrend }}%</span>
                                    @elseif($usersTodayTrend < 0)
                                        <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> {{ $usersTodayTrend }}%</span>
                                    @elseif(($usersTodayTrend == 0))
                                        <span class="text-light mr-2">{{ $usersTodayTrend }}%</span>
                                    @endif
                                    <span class="text-nowrap">Since yesterday</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <a id="transactions_today" href="#">
                        <div class="card card-stats mb-4 mb-xl-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Today transactions</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $transactionsTodayTotal }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                            <i class="fas fa-chart-bar"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    @if ($transactionsTodayTrend > 0)
                                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{ $transactionsTodayTrend }}%</span>
                                    @elseif($transactionsTodayTrend < 0)
                                        <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> {{ $transactionsTodayTrend }}%</span>
                                    @elseif(($transactionsTodayTrend == 0))
                                        <span class="text-light mr-2">{{ $transactionsTodayTrend }}%</span>
                                    @endif
                                    <span class="text-nowrap">Since yesterday</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <a id="rebills_today" href="/statistics">
                        <div class="card card-stats mb-4 mb-xl-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Today rebills</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $rebillsTodayTotal }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-blue text-white rounded-circle shadow">
                                            <i class="fas fa-credit-card"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    @if ($rebillsTodayTrend > 0)
                                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{ $rebillsTodayTrend }}%</span>
                                    @elseif($rebillsTodayTrend < 0)
                                        <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> {{ $rebillsTodayTrend }}%</span>
                                    @elseif(($transactionsWeekTrend == 0))
                                        <span class="text-light mr-2">{{ $rebillsTodayTrend }}%</span>
                                    @endif
                                    <span class="text-nowrap">Since yesterday</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <a id="users_7_days" href="#">
                        <div class="card card-stats mb-4 mb-xl-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Last 7 days users</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $usersWeekTotal }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    @if ($usersWeekTrend > 0)
                                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{ $usersWeekTrend }}%</span>
                                    @elseif($usersWeekTrend < 0)
                                        <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> {{ $usersWeekTrend }}%</span>
                                    @elseif(($usersWeekTrend == 0))
                                        <span class="text-light mr-2">{{ $usersWeekTrend }}%</span>
                                    @endif
                                    <span class="text-nowrap">Since previous 7 days</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <a id="transactions_7_days" href="#">
                        <div class="card card-stats mb-4 mb-xl-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Last 7 days transactions</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $transactionsWeekTotal }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-orange text-white rounded-circle shadow">
                                            <i class="fas fa-chart-bar"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    @if ($transactionsWeekTrend > 0)
                                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{ $transactionsWeekTrend }}%</span>
                                    @elseif($transactionsWeekTrend < 0)
                                        <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> {{ $transactionsWeekTrend }}%</span>
                                    @elseif(($transactionsWeekTrend == 0))
                                        <span class="text-light mr-2">{{ $transactionsWeekTrend }}%</span>
                                    @endif
                                    <span class="text-nowrap">Since previous 7 days</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <a id="rebills_7_days" href="/statistics">
                        <div class="card card-stats mb-4 mb-xl-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Last 7 days rebills</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $rebillsWeekTotal }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-indigo text-white rounded-circle shadow">
                                            <i class="fas fa-credit-card"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    @if ($rebillsWeekTrend > 0)
                                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{ $rebillsWeekTrend }}%</span>
                                    @elseif($rebillsWeekTrend < 0)
                                        <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> {{ $rebillsWeekTrend }}%</span>
                                    @elseif(($rebillsWeekTrend == 0))
                                        <span class="text-light mr-2">{{ $rebillsWeekTrend }}%</span>
                                    @endif
                                    <span class="text-nowrap">Since previous 7 days</span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
