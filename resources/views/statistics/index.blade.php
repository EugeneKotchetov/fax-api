@extends('layouts.app', ['title' => __('Statistics list')])

@section('content')
    @include('layouts.headers.partials.header', [
       'title' => 'Statistics by groups',
       'description' => 'Total Active Transactions: ' . $totalActive,
       'class' => 'col-lg-12'
   ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="ml-auto col-md-2">
                                <select class="form-control form-control-sm" id="selectTimeZone">
                                    <option value="Europe/Moscow">Europe/Moscow</option>
                                    <option value="Asia/Dhaka">UTC+6</option>
                                    <option value="UTC">UTC</option>
                                </select>
                            </div>
                            <div class="ml-auto col-md-2">
                                <select class="form-control form-control-sm" id="selectDateType">
                                    <option value="transaction">Transaction date</option>
                                    <option value="action">Action date</option>
                                </select>
                            </div>
                            <div class="ml-auto col-md-2">
                                <select class="form-control form-control-sm" id="selectGroup">
                                    <option value="day">Group by day</option>
                                    <option value="month">Group by month</option>
                                    <option value="hour">Group by hour</option>
                                    <option value="country">Group by country</option>
                                    <option value="city">Group by city</option>
                                    <option value="os_version">Group by OS version</option>
                                    <option value="apple_id">Group by tariff</option>
                                    <option value="group">Group by tariff group</option>
                                    <option value="version">Group by version</option>
                                    <option value="screen">Group by screen</option>
                                    <option value="store">Group by store</option>
                                </select>
                            </div>
                            <div class="ml-auto col-md-2">
                                <select class="form-control form-control-sm" id="filterByGroup">
                                    <option value="" selected>Filter By Group</option>
                                    @foreach($groups as $group)
                                            <option value="{{ $group }}">{{ $group }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="ml-auto col-md-2">
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 2px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span></span> <i class="fa fa-caret-down"></i>
                                </div>
                            </div>
                            <div class="ml-auto col-md-2">
                                <button type="button" id="btnSettings" class="btn btn-info btn-sm" data-toggle="collapse" data-target="#collapseSettings" aria-expanded="false" aria-controls="collapseSettings">Settings</button>
                                <button type="button" id="btnRefresh" class="btn btn-primary btn-sm"><i class="fas fa-sync-alt"></i></button>
                                <button type="button" id="btnCsv" class="btn btn-default btn-sm ml-auto">CSV</button>
                            </div>
                        </div>
                    </div>
                    <div class="collapse px-4 pb-4" id="collapseSettings">
                        <div class="custom-control-inline font-weight-bold">Show fields: </div>
                        @foreach($fieldNames as $key => $name)
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" class="checkbox-field custom-control-input" name="{{ $key }}" id="{{ $key }}" value="1" {{ isset($fields->$key) && $fields->$key ? 'checked': '' }}>
                                <label class="custom-control-label" for="{{ $key }}">{{ $name }}</label>
                            </div>
                        @endforeach
                    </div>
                    <div class="table-responsive" style="min-height: 150px">

                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <style>
        th {
            cursor: pointer;
        }

        th.sorted[data-order="1"],
        th.sorted[data-order="-1"] {
            position: relative;
        }

        th.sorted[data-order="1"]::after,
        th.sorted[data-order="-1"]::after {
            right: 12px;
            position: absolute;
        }

        th.sorted[data-order="-1"]::after {
            content: "▼"
        }

        th.sorted[data-order="1"]::after {
            content: "▲"
        }
    </style>

    <script type="text/javascript">
        $(function() {

            let start = moment();
            let end = moment();
            let dates = [start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD')];
            let timezone = $('#selectTimeZone').val();
            let datetype = $('#selectDateType').val();
            let group = $('#selectGroup').val();
            let groupFilter = $('#selectGroup').val();

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

            $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                dates = [];
                dates.push(picker.startDate.format('YYYY-MM-DD'));
                dates.push(picker.endDate.format('YYYY-MM-DD'));
            });

            $(document).on('change','#selectTimeZone',function(e){
                timezone = $('#selectTimeZone').val();
            });

            $(document).on('change','#selectDateType',function(e){
                datetype = $('#selectDateType').val();
            });

            $(document).on('change','#selectGroup',function(e){
                group = $('#selectGroup').val();
            });

            $(document).on('change','#filterByGroup',function(e){
                groupFilter = $('#filterByGroup').val();
            });

            function cb(start, end) {
                $('#reportrange span').html(start.format('DD.MM.YY') + ' - ' + end.format('DD.MM.YY'));
            }

            function sortTable() {
                const getSort = ({ target }) => {
                    const order = (target.dataset.order = -(target.dataset.order || -1));
                    const index = [...target.parentNode.cells].indexOf(target);
                    const collator = new Intl.Collator(['en', 'ru'], { numeric: true });
                    const comparator = (index, order) => (a, b) => order * collator.compare(
                        a.children[index].innerHTML,
                        b.children[index].innerHTML
                    );

                    for(const tBody of target.closest('table').tBodies)
                        tBody.append(...[...tBody.rows].sort(comparator(index, order)));

                    for(const cell of target.parentNode.cells)
                        cell.classList.toggle('sorted', cell === target);
                };

                document.querySelectorAll('.table_sort thead').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));
            }

            function getData() {
                let fields = {};
                $('.checkbox-field:checked').each(function(){
                    fields[$(this).attr('name')] = $(this).val();
                });

                const formData = new FormData();
                formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
                formData.append('daterange', JSON.stringify(dates));
                formData.append('timezone', timezone);
                formData.append('datetype', datetype);
                formData.append('group', group);
                formData.append('groupFilter', groupFilter);
                formData.append('fields', JSON.stringify(fields));

                return formData;
            }

            function load() {
                let formData = getData();

                axios.post('/statistics/fetch', formData).then((response) => {
                    $('.table-responsive').html(response.data);
                    sortTable();
                }).catch((error) => {
                    console.log(error);
                });
            }

            $(document).on('click', '#btnRefresh', function(e) {
                load();
            });

            $(document).ready(function() {
                load();
            });

            $(document).on('click', '#btnCsv', function(e) {
                let queryString = new URLSearchParams(getData()).toString();

                document.location.href = `/statistics/csv?${queryString}`;
            });

            $(document).on('click', '.tr-click', function(e) {
                if (group === 'day') {
                    let bgColor = '#F2F2F2';
                    $('.tr-click').css('background', '');

                    // select dates down
                    let subDate = $(this).data('name');
                    while(moment(subDate).isBetween(dates[0], dates[1])) {
                        subDate = moment(subDate).subtract(7, 'days').format('YYYY-MM-DD');
                        $('*[data-name="'+subDate+'"]').css('background', bgColor);
                    }
                    // select dates up
                    let addDate = $(this).data('name');
                    while(moment(addDate).isBetween(dates[0], dates[1])) {
                        addDate = moment(addDate).add(7, 'days').format('YYYY-MM-DD');
                        $('*[data-name="'+addDate+'"]').css('background', bgColor);
                    }
                    //select current date
                    $(this).css('background', bgColor);
                }
            });
        });
    </script>
@endpush
