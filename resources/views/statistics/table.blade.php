<table class="table table-hover align-items-center table-flush table_sort">
    <thead class="thead-light">
    <tr>
        <th scope="col" class="sorted" data-order="1">Group</th>
        @isset($fields->users)<th scope="col">Users</th>@endisset
        @isset($fields->transactions)<th scope="col">Transactions</th>@endisset
        @isset($fields->transactions_late)<th scope="col">Transactions late</th>@endisset
        @isset($fields->transactions_amount)<th scope="col">TS Amount</th>@endisset
        @isset($fields->cr)<th scope="col">CR</th>@endisset
        @isset($fields->cancellations)<th scope="col" style="background-color: #ffdada">Cancellations</th>@endisset
        @isset($fields->cancellations)<th scope="col" style="background-color: #ffdada">C-s Rate</th>@endisset
        @isset($fields->cancellations_late)<th scope="col">Cancellations late</th>@endisset
        @isset($fields->active)<th scope="col">Active</th>@endisset
        @isset($fields->rebills)<th scope="col">Rebills</th>@endisset
        @isset($fields->rebills_amount)<th scope="col">RB Amount</th>@endisset
    </tr>
    </thead>
    <tbody>
    @foreach($statistics as $statistic)
        <tr class="tr-click" data-name="{{ $statistic['name'] }}">
            <td>{{ $statistic['name'] }}</td>
            @isset($fields->users)<td>{{ $statistic['users'] }}</td>@endisset
            @isset($fields->transactions)<td>{{ $statistic['transactions'] }}</td>@endisset
            @isset($fields->transactions_late)<td>{{ $statistic['transactionsLate'] }}</td>@endisset
            @isset($fields->transactions_amount)<td>${{ $statistic['transactionsAmount'] }}</td>@endisset
            @isset($fields->cr)<td>{{ $statistic['ctr'] }}%</td>@endisset
            @isset($fields->cancellations)<td style="background-color: #ffdada">{{ $statistic['cancelled'] }}</td>@endisset
            @isset($fields->cancellations)<td style="background-color: #ffdada">{{ $statistic['transactions'] ? round($statistic['cancelled'] / $statistic['transactions'], 2) * 100 : 0 }}%</td>@endisset
            @isset($fields->cancellations_late)<td>{{ $statistic['cancelledLate'] }}</td>@endisset
            @isset($fields->active)<td>{{ $statistic['active'] }}</td>@endisset
            @isset($fields->rebills)<td>{{ $statistic['renewed'] }}</td>@endisset
            @isset($fields->rebills_amount)<td>${{ $statistic['renewedAmount'] }}</td>@endisset
        </tr>
    @endforeach
    </tbody>
    <tr class="font-weight-bold">
        <td>Total</td>
        @isset($fields->users)<td>{{ $count->users }}</td>@endisset
        @isset($fields->transactions)<td>{{ $count->transactions }}</td>@endisset
        @isset($fields->transactions_late)<td>{{ $count->transactionsLate }}</td>@endisset
        @isset($fields->transactions_amount)<td>${{ $count->transactionsAmount }}</td>@endisset
        @isset($fields->cr)<td>{{ $count->ctr }}%</td>@endisset
        @isset($fields->cancellations)<td style="background-color: #ffdada">{{ $count->cancelled }}</td>@endisset
        @isset($fields->cancellations)<td style="background-color: #ffdada">{{ $count->transactions ? round($count->cancelled/$count->transactions, 2) * 100 : 0 }}%</td>@endisset
        @isset($fields->cancellations_late)<td>{{ $count->cancelledLate }}</td>@endisset
        @isset($fields->active)<td>{{ $count->active }}</td>@endisset
        @isset($fields->rebills)<td>{{ $count->renewed }}</td>@endisset
        @isset($fields->rebills_amount)<td>${{ $count->renewedAmount }}</td>@endisset
    </tr>
</table>
