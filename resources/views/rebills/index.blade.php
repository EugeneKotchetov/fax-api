@extends('layouts.app', ['title' => __('Tariffs list')])

@section('content')
    @include('layouts.headers.partials.header', [
       'title' => 'Rebills Forecast',
       'description' => __('This is list of Rebills Forecast.'),
       'class' => 'col-lg-12'
   ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-md-10">
                                <h3 class="mb-0">Tomorrow rebills forecast</h3>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control form-control-sm" id="selectTimeZone">
                                    <option value="UTC">UTC</option>
                                    <option value="Europe/Moscow">Europe/Moscow</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Days</th>
                                <th scope="col">Transactions</th>
                                <th scope="col">Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rebills as $rebill)
                                <tr>
                                    <td>{{ $rebill['days'] }}</td>
                                    <td>{{ $rebill['transactions'] }}</td>
                                    <td>${{ $rebill['amount'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tr class="font-weight-bold">
                                <td>Total</td>
                                <td>{{ $totalCount }}</td>
                                <td>${{ $totalAmount }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer py-4">

                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(function() {
            let timezone = '{!! $timeZone !!}';
            $('#selectTimeZone').val(timezone);

            $(document).on('change','#selectTimeZone',function(e){
                timezone = $('#selectTimeZone').val();
                window.location = `rebills?timezone=${timezone}`;
            });
        });
    </script>
@endpush
