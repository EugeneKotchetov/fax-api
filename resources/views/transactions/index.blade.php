@extends('layouts.app', ['title' => __('Transactions list')])

@section('content')
    @include('layouts.headers.partials.header', [
       'title' => 'Transactions list',
       'description' => __('This is list of transactions page.'),
       'class' => 'col-lg-12'
   ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-md-2">
                                <h3 class="mb-0">Total {{ $transactions->total() }}</h3>
                            </div>
                            <div class="ml-auto col-md-10">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div id="reportrange"
                                         style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 40%">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fa fa-caret-down"></i>
                                    </div>
                                    <div class="ml-3 custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkShowActive">
                                        <label class="custom-control-label" for="checkShowActive">Show Active
                                            Transactions</label>
                                    </div>
                                    <div class="ml-3 custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkShowAS">
                                        <label class="custom-control-label" for="checkShowAS">Show AS</label>
                                    </div>
                                    <div class="ml-3 custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkShowPM">
                                        <label class="custom-control-label" for="checkShowPM">Show PM</label>
                                    </div>
                                    <button type="button" id="btnCsv" class="btn btn-default btn-sm ml-auto">CSV
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">User Id</th>
                                <th scope="col">Store</th>
                                <th scope="col" id="orderUser">User created date</th>
                                <th scope="col">Country</th>
                                <th scope="col">Transaction_id</th>
                                <th scope="col">Amount</th>
                                <th scope="col" id="orderPurchase">Purchase date</th>
                                <th scope="col">Renew date</th>
                                <th scope="col">Re-bills count</th>
                                <th scope="col">Cancellation date</th>
                                <th scope="col">Days Live</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->id }}</td>
                                    <td>{{ $transaction->user_id }}</td>
                                    <td>{{ $transaction->store }}</td>
                                    <td>{{ $transaction->user ? $transaction->user->created_at : '' }}</td>
                                    <td>{{ $transaction->user ? $transaction->user->country : '-' }}</td>
                                    <td>{{ $transaction->transaction_id == 1 ? $transaction->order_id :  $transaction->transaction_id }}</td>
                                    <td>{{ $transaction->tariff ? '$' . $transaction->tariff->price : '' }}</td>
                                    <td>{{ $transaction->purchase_date }}</td>
                                    <td>
                                        @if ($transaction->is_renewed)
                                            <span
                                                class="badge badge-pill badge-success">{{ $transaction->renewed_date }}</span>
                                        @endif
                                    </td>
                                    <td>{{ $transaction->rebills_count }}</td>
                                    <td>
                                        @if ($transaction->is_cancelled)
                                            <span
                                                class="badge badge-pill badge-danger">{{ $transaction->cancelled_date }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($transaction->is_cancelled)
                                            {{ $transaction->purchase_date->diffInDays($transaction->cancelled_date) }}
                                        @else
                                            {{ $transaction->purchase_date->diffInDays(\Carbon\Carbon::now()) }}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tr class="font-weight-bold">
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $transactions_amount ? '$' . $transactions_amount : '' }}</td>
                                <td></td>
                                <td></td>
                                <td>{{ $rebills_count }}</td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        {{ $transactions->withQueryString()->links('layouts.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(function () {

            $('#orderUser').css({'text-decoration': 'underline', 'cursor': 'pointer'});
            $('#orderPurchase').css({'text-decoration': 'underline', 'cursor': 'pointer'});

            let start = moment('{!! $daterange[0] !!}');
            let end = moment('{!! $daterange[1] !!}');
            let dates = [start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD')];

            $('#checkShowActive').prop('checked', {!! $showactive !!});
            let showActive = $('#checkShowActive').is(':checked') ? true : false;

            $('#checkShowAS').prop('checked', {!! $showas !!});
            let showAS = $('#checkShowAS').is(':checked') ? true : false;

            $('#checkShowPM').prop('checked', {!! $showpm !!});
            let showPM = $('#checkShowPM').is(':checked') ? true : false;

            let order = '{!! $order !!}';
            let orderDirection = '{!! $order_direction !!}';
            let orderSign = orderDirection === 'ASC' ? '▼' : '▲';
            switch (order) {
                case 'user':
                    $('#orderUser').text('User created date ' + orderSign);
                    $('#orderPurchase').text('Purchase date');
                    break;
                case 'purchase':
                    $('#orderUser').text('User created date');
                    $('#orderPurchase').text('Purchase date ' + orderSign);
                    break;
            }

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                dates = [];
                dates.push(picker.startDate.format('YYYY-MM-DD'));
                dates.push(picker.endDate.format('YYYY-MM-DD'));
                window.location = `transactions?daterange=${encodeURIComponent(JSON.stringify(dates))}&showactive=${showActive}&showas=${showAS}&showpm=${showPM}&order=${order}&orderdirection=${orderDirection}`;
            });

            $(document).on('click', '#checkShowActive', function () {
                showActive = $('#checkShowActive').is(':checked') ? true : false;
                window.location = `transactions?daterange=${encodeURIComponent(JSON.stringify(dates))}&showactive=${showActive}&showas=${showAS}&showpm=${showPM}&order=${order}&orderdirection=${orderDirection}`;
            });

            $(document).on('click', '#checkShowAS', function () {
                showAS = $('#checkShowAS').is(':checked') ? true : false;
                window.location = `transactions?daterange=${encodeURIComponent(JSON.stringify(dates))}&showactive=${showActive}&showas=${showAS}&showpm=${showPM}&order=${order}&orderdirection=${orderDirection}`;
            });

            $(document).on('click', '#checkShowPM', function () {
                showPM = $('#checkShowPM').is(':checked') ? true : false;
                window.location = `transactions?daterange=${encodeURIComponent(JSON.stringify(dates))}&showactive=${showActive}&showas=${showAS}&showpm=${showPM}&order=${order}&orderdirection=${orderDirection}`;
            });

            $(document).on('click', '#btnCsv', function () {
                window.location = `transactions/csv?daterange=${encodeURIComponent(JSON.stringify(dates))}&showactive=${showActive}&showas=${showAS}&showpm=${showPM}&order=${order}&orderdirection=${orderDirection}`;
            });

            $(document).on('click', '#orderUser', function () {
                order = 'user';
                if (orderDirection === 'ASC') {
                    orderDirection = 'DESC';
                } else {
                    orderDirection = 'ASC';
                }
                window.location = `transactions?daterange=${encodeURIComponent(JSON.stringify(dates))}&showactive=${showActive}&showas=${showAS}&showpm=${showPM}&order=${order}&orderdirection=${orderDirection}`;
            });

            $(document).on('click', '#orderPurchase', function () {
                order = 'purchase';
                if (orderDirection === 'ASC') {
                    orderDirection = 'DESC';
                } else {
                    orderDirection = 'ASC';
                }
                window.location = `transactions?daterange=${encodeURIComponent(JSON.stringify(dates))}&showactive=${showActive}&showas=${showAS}&showpm=${showPM}&order=${order}&orderdirection=${orderDirection}`;
            });
        });
    </script>
@endpush
