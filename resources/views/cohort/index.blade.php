@extends('layouts.app', ['title' => __('Cohort analysis')])

@section('content')
    @include('layouts.headers.partials.header', [
       'title' => 'Cohort analysis',
       'description' => __('This is list of cohort analysis.'),
       'class' => 'col-lg-12'
   ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <form method="post">
                        <div class="row align-items-center">


                                @csrf
                                <div class="col-md-2">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="checkbox-field custom-control-input"
                                               name="amount70" id="amount70" value="1"  {{ request('amount70') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="amount70">Amount - 30%</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="checkbox-field custom-control-input"
                                               name="amount85" id="amount85" value="1" {{ request('amount85') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="amount85">Amount - 15%</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="checkbox-field custom-control-input" name="rebill"
                                               id="rebill" value="1" {{ request('rebill') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="rebill">Include rebills in initial
                                            month</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" id="btnRefresh" class="btn btn-primary btn-sm"><i
                                                class="fas fa-sync-alt"></i></button>
                                </div>

                        </div>
                        </form>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col"></th>
                                @foreach($months as $month)
                                    <th scope="col" style="font-size: 11px;">{{ $month }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rows as $row)
                                <tr>
                                @foreach($row as $col)
                                    @if ($loop->first)
                                        <td style="background: #F6F9FC; text-transform: uppercase; font-size: 11px;">{{ $col }}</td>
                                    @else
                                        <td>{{ $col }}</td>
                                    @endif
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
