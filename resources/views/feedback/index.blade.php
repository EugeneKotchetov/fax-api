@extends('layouts.app', ['title' => __('Feedback list')])

@section('content')
    @include('layouts.headers.partials.header', [
       'title' => 'Users feedback',
       'description' => __('This is list of users feedback.'),
       'class' => 'col-lg-12'
   ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <h3 class="mb-0">Feedback list</h3>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">User ID</th>
                                <th scope="col">Is user paid</th>
                                <th scope="col">Rate</th>
                                <th scope="col">Text</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($feedbacks as $feedback)
                                <tr>
                                    <td>{{ $feedback->user->id }}</td>
                                    <td>
                                        @if ($feedback->user->is_paid)
                                            <span class="badge badge-pill badge-success">Yes</span>
                                        @else
                                            <span class="badge badge-pill badge-danger">No</span>
                                        @endif
                                    </td>
                                    <td>{{ $feedback->rate }}</td>
                                    <td>{{ $feedback->text }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        {{ $feedbacks->withQueryString()->links('layouts.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
