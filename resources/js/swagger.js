require('./bootstrap');

var SwaggerUIBundle = require('swagger-ui-dist').SwaggerUIBundle;

const ui = SwaggerUIBundle({
    url: "/swagger-doc/app.yaml",
    dom_id: '#swagger-ui',
    presets: [
        SwaggerUIBundle.presets.apis,
        SwaggerUIBundle.SwaggerUIStandalonePreset
    ],
    plugins: [
        SwaggerUIBundle.plugins.DownloadUrl
    ],
    //layout: "StandaloneLayout"
})

window.onload = function() {
    window.ui = ui
}
