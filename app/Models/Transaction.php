<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Log;

/**
 * Class Transaction
 *
 * @property int    $id
 * @property int    $user_id
 * @property int    $original_transaction_id
 * @property int    $transaction_id
 * @property string $original_order_id
 * @property string $order_id
 * @property string $tariff_apple_id
 * @property string $receipt
 * @property Carbon $purchase_date
 * @property boolean $is_cancelled
 * @property boolean $is_renewed
 * @property Carbon $renewed_date
 * @property Carbon $cancelled_date
 * @property int    $rebills_count
 * @property int    $type
 * @property string $store
 * @property string $purchase_token
 *
 * @property string $created_at
 * @property string $updated_at
 *
 * @property-read User $user
 *
 * @package App\Models
 */

class Transaction extends Model
{
    const TYPE_ORIGINAL = 1;
    const TYPE_RE_BILL  = 2;
    const TYPE_RESTORE  = 3;

    const STORE_APPLE = 'AS';
    const STORE_GOOGLE = 'PM';

    const PLAY_MARKET_PACKAGE = 'com.siilingou.fax';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'original_transaction_id', 'transaction_id', 'tariff_apple_id', 'receipt', 'purchase_date'
    ];

    /**
     * The attributes that should be date types.
     *
     * @var array
     */
    protected $dates = ['purchase_date', 'renewed_date', 'cancelled_date'];

    protected $perPage = 50;

    public function saveFromGoogleResponse($receiptRaw, $userId)
    {
        $transaction = json_decode($receiptRaw);

        if (isset($transaction->orderId) && isset($transaction->purchaseToken)) {
            $this->store = self::STORE_GOOGLE;
            $this->order_id = $transaction->orderId;
            $this->original_order_id = $transaction->orderId;
            $this->transaction_id = 1;
            $this->original_transaction_id = 1;
            $this->user_id = $userId;
            $this->purchase_token = $transaction->purchaseToken;
            $this->tariff_apple_id = $transaction->productId;
            $this->purchase_date = date('Y-m-d H:i:s', $transaction->purchaseTime / 1000);
            $this->receipt = $receiptRaw;
            $this->save();

            return true;
        }

        return false;
    }

    public function saveFromAppleResponse($response, $userId, $receiptRaw = null): bool
    {
        try {
            if (isset($response->receipt) && isset($response->receipt->in_app) && isset($response->receipt->in_app[0])
                && $transaction = $response->receipt->in_app[0]) {

                if ($transaction->original_transaction_id == '230000844506596' || $transaction->transaction_id == '230000844506596') {
                    die();
                }
                $this->store = self::STORE_APPLE;
                $this->transaction_id = $transaction->transaction_id;
                $this->original_transaction_id = $transaction->original_transaction_id;
                $this->user_id = $userId;
                $this->tariff_apple_id = $transaction->product_id;
                $this->purchase_date = date('Y-m-d H:i:s', $transaction->purchase_date_ms / 1000);
                $this->receipt = $receiptRaw;
                $this->save();

                return true;
            }

        } catch (\Exception $e) {
            Log::error(print_r($response, true));
            Log::error($e->getMessage() . ':' . $e->getTraceAsString());
        }

        return false;
    }

    public function saveRebillFromAppleResponse($response, $userId, $receiptRaw = null): bool
    {
        try {
            if (isset($response->latest_receipt_info) && isset($response->latest_receipt_info[0])
                && $transaction = $response->latest_receipt_info[0]) {

                $this->transaction_id = $transaction->transaction_id;
                $this->original_transaction_id = $transaction->original_transaction_id;
                $this->user_id = $userId;
                $this->tariff_apple_id = $transaction->product_id;
                $this->purchase_date = date('Y-m-d H:i:s', $transaction->purchase_date_ms / 1000);
                $this->receipt = $receiptRaw;
                $this->type = self::TYPE_RE_BILL;
                $this->save();

                return true;
            }

        } catch (\Exception $e) {
            Log::error(print_r($response, true));
            Log::error($e->getMessage() . ':' . $e->getTraceAsString());
        }

        return false;
    }

    public function getTariffAppleId(): string
    {
        return str_replace('com.siilinou.faxapp.', '', $this->tariff_apple_id);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeNotCancelled(Builder $query)
    {
        $query->where('is_cancelled', false)
            ->where('type', self::TYPE_ORIGINAL);
    }

    public function scopeRenewToday(Builder $query)
    {
        $query->join('users', 'users.id', '=', 'transactions.user_id');

        $startTime = time() - 3600*6;
        $endTime = time() + 3600*6;

        $query->whereBetween('users.paid_until', [$startTime, $endTime]);
    }

    public function scopeHasStoreTypes(Builder $query, array $storeTypes)
    {
        if (count($storeTypes)) {
            $query->whereIn('store', $storeTypes);
        }
    }

    public function cancel(): void
    {
        $this->is_cancelled = true;
        $this->cancelled_date = Carbon::now();
    }

    public function renew($timestamp, $receipt): void
    {
        $paidUntil = Carbon::createFromTimestampMs($timestamp);

        $this->is_renewed = true;
        $this->renewed_date = Carbon::now();
        $this->rebills_count++;

        try {
            $user = $this->user;
            $user->paid_until = $paidUntil->timestamp;
            $user->save();
        } catch (\Exception $e) {
            echo '[-] Cant change user' . PHP_EOL;
        }

        // сохраним ребилл отдельно
        $rebillTransaction = new Transaction();
        $rebillTransaction->saveRebillFromAppleResponse($receipt, $this->user_id);
    }

    public function tariff(): BelongsTo
    {
        return $this->belongsTo(Tariff::class, 'tariff_apple_id', 'apple_id');
    }

}
