<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class FaxFiles
 *
 * @property int $id
 * @property int $fax_id
 * @property int $page
 * @property string $file_name
 * @property string $file_path
 * @property string $fax_sid
 * @property string $status
 *
 * @property string $created_at
 * @property string $updated_at
 *
 * @property-read Fax $fax
 *
 * @package App\Models
 */
class FaxFiles extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fax_id', 'page', 'file_name', 'file_path'
    ];

    public function setFax(Fax $fax)
    {
        $this->fax_id = $fax->id;
    }

    public function fax(): BelongsTo
    {
        return $this->belongsTo(Fax::class);
    }

}
