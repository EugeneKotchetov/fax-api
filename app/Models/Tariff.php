<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tariff
 *
 * @property int    $id
 * @property string $title
 * @property string $apple_id
 * @property string $description
 * @property string $discount
 * @property boolean $by_default
 * @property float $price
 * @property int $days
 * @property boolean $is_active
 * @property int $sort
 * @property int $group
 *
 * @property string $created_at
 * @property string $updated_at
 *
 * @package App\Models
 */

class Tariff extends Model
{
    const TRIAL_DAYS = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'apple_id', 'description', 'discount', 'by_default', 'price', 'days', 'is_active', 'sort', 'group',
    ];

}
