<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Fax
 *
 * @property int    $id
 * @property int    $user_id
 * @property string $dst
 * @property Carbon $sent_at
 * @property string $fax_sid
 * @property string $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property int    $attempt
 * @property string $name
 *
 * @property-read FaxFiles[] $files
 * @property-read FaxFiles[] $filesToCheck
 *
 * @package App\Models
 */
class Fax extends Model
{
    use SoftDeletes;

    /** @var array  */
    public static $successStatuses = ['delivered', 'received', 'Sent'];

    /** @var array  */
    public static $failedStatuses = ['failed', 'no-answer', 'busy', 'canceled', 'Failed'];

    public function __construct(array $attributes = [])
    {
        $this->casts['sent_at'] = 'datetime';

        parent::__construct($attributes);
    }

    public function files(): HasMany
    {
        return $this->hasMany(FaxFiles::class);
    }

    public function filesToCheck(): HasMany
    {
        return $this->hasMany(FaxFiles::class)
            ->where('status', null);
    }

    public function markAsSent(): void
    {
        $this->sent_at = Carbon::now();
    }

    public function scopeToCheck(Builder $query): Builder
    {
        return $query->where('status', null)
            ->where('sent_at', '<=', Carbon::now()->subMinutes(15));
    }

    public function getStatus(): string
    {
        return $this->status ?? 'pending';
    }

    public function checkStatus(): ?string
    {
        $total = count($this->files);
        $totalToCheck = count($this->filesToCheck);

        echo '[*] Working on fax #' . $this->id . '#, total files=' . $total . ', files to check=' . $totalToCheck . PHP_EOL;

        // значит все файлы проверили и они получили финальный статус
        if ($totalToCheck == 0) {

            $successFilesCount = $this->files()
                ->whereIn('status', self::$successStatuses)
                ->count();

            if ($successFilesCount == $total) {
                return 'success';
            }

            $failedFilesCount = $this->files()
                ->whereIn('status', self::$failedStatuses)
                ->count();


            if ($failedFilesCount == $total) {
                return 'failed';
            }

            return 'partial success';
        }

        return null;
    }
}
