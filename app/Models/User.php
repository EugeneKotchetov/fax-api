<?php

namespace App\Models;

use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Boolean;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class User
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $os_version
 * @property string $ip
 * @property string $country
 * @property string $city
 * @property string $connection
 * @property string $proxy_type
 * @property boolean $is_active
 * @property int $tariff_id
 * @property int $paid_until
 * @property float $payment_amount
 * @property string $tariff_list
 * @property string $push_token
 * @property int $notification_status
 * @property Carbon $notificated_at
 * @property string $version
 * @property integer $screen
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'os_version',
        'ip',
        'country',
        'city',
        'connection',
        'proxy_type',
        'is_active',
        'tariff_id',
        'paid_until',
        'payment_amount',
        'tariff_list',
        'screen',
    ];

    protected $perPage = 50;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'notificated_at',
    ];

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array|string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->username;
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }

    public function getIsPaidAttribute(): Bool
    {
        if ($this->transactions()->count()) {
            return true;
        }
        return false;
    }

    public function scopeHasPaid(Builder $query): Builder
    {
        return $query->has('transactions');
    }

    public function getShortUsername(): string
    {
        return substr($this->username, 0, 8);
    }
}
