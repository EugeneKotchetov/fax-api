<?php

namespace App\Console\Commands;

use App\Models\Tariff;
use App\Models\Transaction;
use App\Services\AppleService;
use Illuminate\Console\Command;
use Imdhemy\GooglePlay\ClientFactory;
use Imdhemy\GooglePlay\Subscriptions\Subscription;


class CheckCancellation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:cancellation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check subscription cancellations';

    /** @var AppleService */
    private $appleService;

    /**
     * Create a new command instance.
     *
     * @param AppleService $appleService
     * @return void
     */
    public function __construct(AppleService $appleService)
    {
        $this->appleService = $appleService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var Transaction[] $transactions */
        $transactions = Transaction::query()->notCancelled()->get();

        foreach ($transactions as $transaction) {
            $this->info('[+] Trying to update status of transaction #' . $transaction->id . '..');

            if ($transaction->store == Transaction::STORE_GOOGLE) {
                //$this->verifyGoogle($transaction);
            } else {
                $this->verifyApple($transaction);
            }
        }
    }

    /**
     * @param Transaction $transaction
     *

    [kind:protected] => androidpublisher#subscriptionPurchase
    [startTimeMillis:protected] => 1608397585852
    [expiryTimeMillis:protected] => 1608398120819
    [autoResumeTimeMillis:protected] =>
    [autoRenewing:protected] => 1
    [priceCurrencyCode:protected] => RUB
    [priceAmountMicros:protected] => 639000000
    [introductoryPriceInfo:protected] =>
    [countryCode:protected] => RU
    [developerPayload:protected] =>
    [paymentState:protected] => 1
    [cancelReason:protected] =>
    [userCancellationTimeMillis:protected] =>
    [cancelSurveyResult:protected] =>
    [orderId:protected] => GPA.3311-7962-5695-02145
    [linkedPurchaseToken:protected] =>
    [purchaseType:protected] => 0
    [priceChange:protected] =>
    [emailAddress:protected] =>
    [givenName:protected] =>
    [profileId:protected] =>
    [acknowledgementState:protected] => 1
    [externalAccountId:protected] =>
    [promotionType:protected] =>
    [promotionCode:protected] =>
    [obfuscatedExternalAccountId:protected] =>

     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function verifyGoogle(Transaction $transaction)
    {
        $path = base_path() . '/service_acc.json';
        putenv(sprintf("GOOGLE_APPLICATION_CREDENTIALS=%s", $path));

        $client = ClientFactory::create([ClientFactory::SCOPE_ANDROID_PUBLISHER]);
        $subscription = new Subscription($client, Transaction::PLAY_MARKET_PACKAGE, $transaction->tariff_apple_id, $transaction->purchase_token);
        //$subscription->acknowledge();
        $resource = $subscription->get(); // Imdhemy\GooglePlay\Subscriptions\SubscriptionPurchase
        if (!$resource->isAutoRenewing()) {
            $transaction->cancel();
            $transaction->save();
        }
    }

    private function verifyApple(Transaction $transaction)
    {
        $receipt = $this->appleService->verifyReceipt($transaction->receipt);

        if (
            isset($receipt->pending_renewal_info[0])
            && isset($receipt->pending_renewal_info[0]->auto_renew_status)
            && $receipt->pending_renewal_info[0]->auto_renew_status == 0
        ) {
            $transaction->cancel();
            $transaction->save();

            $this->info('[-] Transaction #'.$transaction->id . ' cancelled');
        } else {

            if (!isset($receipt->latest_receipt_info[0])) {
                $this->info('[-] Transaction #'.$transaction->id . ' receipt not found');
                return;
            }

            $latest = $receipt->latest_receipt_info[0];
            if (
                $latest->transaction_id != $latest->original_transaction_id
                && !isset($latest->cancellation_date)
                && $transaction->transaction_id != $latest->transaction_id
            ) {

                $transaction->renew($latest->expires_date_ms, $receipt);
                $transaction->transaction_id = $latest->transaction_id;
                $transaction->save();

                $this->info('[-] Transaction #'.$transaction->id . ' renewed!');
            }
        }
    }
}
