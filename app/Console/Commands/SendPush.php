<?php

namespace App\Console\Commands;

use App\Services\PushService;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Console\Command;

class SendPush extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push';

    /** @var PushService */
    private $pushService;

    /**
     * Create a new command instance.
     *
     * @param PushService $pushService
     *
     * @return void
     */
    public function __construct(PushService $pushService)
    {
        parent::__construct();
        $this->pushService = $pushService;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $usersQuery = User::query()
            ->doesntHave('transactions')
            ->whereNotNull('push_token')
            ->whereNotificationStatus(0)
            ->whereDate('created_at', '>', Carbon::now()->subDays(7));

        $tokens = $usersQuery->pluck('push_token')->toArray();

        $result = $this->pushService->send($tokens);

        $usersQuery->update(['notification_status' => 1]);

        $this->info('[+] Sent to ' . count($tokens));
        print_r($result);
    }
}
