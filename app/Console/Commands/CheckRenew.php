<?php

namespace App\Console\Commands;

use App\Models\Transaction;
use App\Services\AppleService;
use Illuminate\Console\Command;

class CheckRenew extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:renew';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check subscription renew';

    /** @var AppleService */
    private $appleService;

    /**
     * Create a new command instance.
     *
     * @param AppleService $appleService
     * @return void
     */
    public function __construct(AppleService $appleService)
    {
        $this->appleService = $appleService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var Transaction[] $transactions */
        $transactions = Transaction::query()->notCancelled()->renewToday()->get();

        foreach ($transactions as $transaction) {
            $this->info('[+] Trying to renew transaction #' . $transaction->id . '..');

            $receipt = $this->appleService->verifyReceipt($transaction->receipt);
            if (!isset($receipt->latest_receipt_info[0])) {
                $this->info('[-] Transaction #'.$transaction->id . ' receipt not found');
                continue;
            }

            $latest = $receipt->latest_receipt_info[0];
            if (
                $latest->transaction_id != $latest->original_transaction_id
                && !isset($latest->cancellation_date)
                && $transaction->transaction_id != $latest->transaction_id
            ) {

                $transaction->renew($latest->expires_date_ms, $receipt);
                $transaction->transaction_id = $latest->transaction_id;
                $transaction->save();

                $this->info('[-] Transaction #'.$transaction->id . ' renewed!');
            }
        }
    }
}
