<?php

namespace App\Console\Commands;

use App\Models\Fax;
use App\Services\FaxService;
use Illuminate\Console\Command;

class SendFax extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fax:send {faxId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Fax #';

    /**
     * @var FaxService
     */
    private $faxService;

    /**
     * Create a new command instance.
     * @param FaxService $faxService
     * @return void
     */
    public function __construct(FaxService $faxService)
    {
        $this->faxService = $faxService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return int
     */
    public function handle()
    {
        /** @var Fax $fax */
        $fax = Fax::query()->where('id', $this->argument('faxId'))->first();
        if ($fax) {
            $status = $this->faxService->send($fax);
            echo '[+] Sending fax status is ' . $status . PHP_EOL;
        } else {
            echo '[-] Fax not found' . PHP_EOL;
        }
    }
}
