<?php

namespace App\Console\Commands;

use App\Models\Fax;
use App\Services\FaxService;
use Illuminate\Console\Command;

class ReSendFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fax:resend {faxId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Try to re-send partial success faxes';

    /**
     * @var FaxService
     */
    private $faxService;

    /**
     * Create a new command instance.
     * @param FaxService $faxService
     * @return void
     */
    public function __construct(FaxService $faxService)
    {
        $this->faxService = $faxService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return int
     */
    public function handle()
    {
        /** @var Fax $fax */
        $fax = Fax::query()->where('id', $this->argument('faxId'))->first();
        if ($fax) {

            foreach ($fax->files as $file) {
                if (in_array($file->status, ['no-answer', 'busy'])) {
                    $this->faxService->resend($file);

                    $file->refresh();
                    $file->page = 0;
                    $file->save();

                    echo '[+] Sent file #'.$file->id.'..' . PHP_EOL;

                    $fax = $file->fax;
                    $fax->status = null;
                    $fax->save();
                }
            }

        } else {
            echo '[-] Fax not found' . PHP_EOL;
        }
    }
}
