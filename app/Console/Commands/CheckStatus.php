<?php

namespace App\Console\Commands;

use App\Models\Fax;
use App\Models\FaxFiles;
use App\Services\FaxService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Fax Statuses';

    /**
     * @var FaxService
     */
    private $faxService;

    /**
     * Create a new command instance.
     * @param FaxService $faxService
     * @return void
     */
    public function __construct(FaxService $faxService)
    {
        $this->faxService = $faxService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var Fax[] $faxes */
        $faxes = Fax::toCheck()->get();
        foreach ($faxes as $fax) {

            try {
                foreach ($fax->filesToCheck as $file) {
                    $this->processFile($file);
                }

                if ($status = $fax->checkStatus()) {

                    $resend = false;
                    if ($status == 'partial success') {

                        foreach ($fax->files as $file) {

                            if (count($fax->files) >= $file->page && $file->page <= 5) {

                                if (in_array(strtolower($file->status), ['no-answer', 'busy', 'failed'])) {

                                    $resend = true;

                                    $file->page++;
                                    $file->save();

                                    $this->faxService->resend($file);

                                    $fax->sent_at = Carbon::now();
                                    $fax->save();

                                    echo '[+] Partial success.. Resend file #' . $file->id . '..' . PHP_EOL;
                                    continue;

                                }
                            }

                        }
                    }

                    if ($resend == false) {
                        $fax->status = $status;
                        $fax->save();
                    }

                }
            } catch (\Exception $e) {
                $this->info('[-] Something goes wrong with Fax ID ' . $fax->id);
            }
        }
    }

    private function processFile(FaxFiles $faxFiles)
    {
        $statusesToSave = array_merge(Fax::$successStatuses, Fax::$failedStatuses);

        if (!$faxFiles->fax_sid) {
            return;
        }

        $status = $this->faxService->getStatus($faxFiles->fax_sid);
        if (in_array($status, $statusesToSave)) {
            $faxFiles->status = $status;
            $faxFiles->save();
        }
    }
}
