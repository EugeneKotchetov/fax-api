<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\GeoIpService;
use Illuminate\Console\Command;

class UpdateCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cities by IP in users table';

    /** @var GeoIpService */
    private $geoIpService;

    /**
     * Create a new command instance.
     *
     * @param GeoIpService $geoIpService
     */
    public function __construct(GeoIpService $geoIpService)
    {
        $this->geoIpService = $geoIpService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();

        foreach ($users as $user) {
            $geo = $this->geoIpService->getGeoByIp($user->ip);

            $user->city = $geo['city'];
            $user->save();

            $this->info($user->id . ' - ' . $geo['city']);
        }

        $this->info('Update successful!');
    }
}
