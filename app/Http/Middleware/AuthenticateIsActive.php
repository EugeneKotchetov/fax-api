<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class AuthenticateIsActive extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     *
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->guard('api')->user();

        if (isset($user) && !$user->is_active) {
            return response()->json(['error' => 'User is not active.'], 403);
        }

        return $next($request);
    }
}
