<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class CheckVersion extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     *
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->guard('api')->user();

        $headerVersion = request()->header('Version');

        if (isset($headerVersion) && isset($user) && $headerVersion != '' && $headerVersion != $user->version) {
            $user->version = $headerVersion;
            $user->save();
        }

        return $next($request);
    }
}
