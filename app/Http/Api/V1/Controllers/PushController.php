<?php

namespace App\Http\Api\V1\Controllers;

use Illuminate\Http\Request;

class PushController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/v1/token",
     *     tags={"pushToken"},
     *     summary="Store an push token",
     *     description="Store an push token",
     *     operationId="StorePushToken",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *              required={"push_token"},
     *                 @OA\Property(
     *                     property="push_token",
     *                     type="string",
     *                     example="SDdferjekf49559kkakk..."
     *                ),
     *          )
     *     ),
     *     @OA\Response(response="200",
     *         description="Push token store successful",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example="true"
     *                 ),
     *                 @OA\Property(
     *                      property="message",
     *                      type="string",
     *                      example="Push token has been stored"
     *                 ),
     *             ),
     *         ),
     *     ),
     *     @OA\Response(response=422, description="Error: Unprocessable Entity")
     * )
     */

    /**
     * Store a push token to user.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeToken(Request $request)
    {
        $this->validate($request, [
            'push_token'  => 'required|string',
        ]);

        $user = $this->guard()->user();

        $user->push_token = $request->push_token;
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Push token has been stored'
        ]);
    }
}
