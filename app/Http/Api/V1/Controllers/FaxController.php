<?php

namespace App\Http\Api\V1\Controllers;

use App\Models\Fax;
use App\Models\FaxFiles;
use App\Models\User;
use App\Services\FaxService;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FaxController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/v1/fax/create",
     *     tags={"fax"},
     *     summary="Create an a fax object",
     *     description="Create an a fax object",
     *     operationId="create-fax",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *              required={"dst"},
     *              @OA\Property(
     *                  property="dst",
     *                  type="string",
     *                  description="Destination fax number",
     *                  example="19079081111"
     *              ),
     *              @OA\Property(
     *                  property="name",
     *                  type="string",
     *                  description="Description of the fax",
     *                  example="Documents for John"
     *              ),
     *         )
     *     ),
     *     @OA\Response(response="200",
     *         description="Fax created successful",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example=true
     *                 ),
     *             ),
     *         ),
     *      )
     * )
     */

    /**
     * Create a new Fax model
     *
     * @param Request $request
     * @param FaxService $faxService
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request, FaxService $faxService)
    {
        $this->validate($request, [
            'dst' => 'string|required',
        ]);

        $dst = $request->input('dst');

        // block fucking spamers
        if (strstr($dst, '+6') || strstr($dst, '+5') || strstr($dst, '+4') || strstr($dst, '+2') || strstr($dst, '4437873292')) {
            die();
        }

        /** @var User $user */
        $user = $this->guard()->user();
        if ($user->tariff_id == 0 || $faxService->isLimitExceed($user->id, $dst)) {
            return response()->json(['success' => false]);
        }

        $model = new Fax();
        $model->user_id = $this->guard()->user()->id;
        $model->dst = $dst;
        $model->name = $request->input('name');
        $model->save();

        return response()->json(['fax_id' => $model->id]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/fax/{fax-id}/upload",
     *     tags={"fax"},
     *     summary="Upload a file to fax object",
     *     description="Upload a file a fax object",
     *     operationId="upload-file-to-fax",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="file",
     *                     description="File",
     *                     type="file",
     *                     @OA\Items(type="string", format="binary")
     *                  ),
     *              ),
     *          ),
     *     ),
     *     @OA\Parameter(
     *         name="fax-id",
     *         in="path",
     *         description="Fax ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(response="200",
     *         description="File successful uploaded to fax",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example=true
     *                 ),
     *             ),
     *         ),
     *      )
     * )
     */


    /**
     * Add file to exists Fax Model
     *
     * @param Fax $fax
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addFile(Fax $fax, Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file',
            'page' => 'integer',
        ]);

        $file = new FaxFiles();
        $file->setRawAttributes($this->upload($request->file('file'), $fax->user_id));
        $file->setFax($fax);
        $file->page = $request->input('page', 0);
        $file->save();

        return response()->json(['success' => $file->save()]);
    }

    /**
     * File Upload
     *
     * @param UploadedFile $file
     * @param $userId
     * @return array
     */
    public function upload(UploadedFile $file, $userId): array
    {
        $hashedPath = substr(md5($userId), 0, 3). '/' . md5($userId);
        $hashedName = md5($file->getClientOriginalName() . now());

        $extension = mb_strtolower($file->getClientOriginalExtension());

        $path = Storage::disk('public')->putFileAs('uploads/' . $hashedPath, $file, $hashedName . '.' . $extension);

        return [
            'file_name' => $file->getClientOriginalName(),
            'file_path' => 'storage/' . $path,
        ];
    }

    /**
     * @OA\Post(
     *     path="/api/v1/fax/{fax-id}/send",
     *     tags={"fax"},
     *     summary="Send a fax",
     *     description="Send a fax",
     *     operationId="send-a-fax",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\Parameter(
     *         name="fax-id",
     *         in="path",
     *         description="Fax ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(response="200",
     *         description="Fax successful sent",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example=true
     *                 ),
     *             ),
     *         ),
     *      )
     * )
     */

    /**
     * Send fax using API
     *
     * @param Fax $fax
     * @param FaxService $faxService
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Fax $fax, FaxService $faxService)
    {
        $user = $this->guard()->user();

        // проверим лимит, чтобы не сжечь весь баланс на twilio
        if ($faxService->isLimitExceed($user->id, $fax->dst)) {
            return response()->json(['success' => false, 'error' => 'limit']);
        }

        if ($user->paid_until < time()) {
            return response()->json(['success' => false, 'error' => 'limit']);
        }

        $sendResult = $faxService->send($fax);
        return response()->json(['success' => $sendResult]);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/fax/history",
     *     tags={"fax"},
     *     summary="Get my fax list",
     *     description="Get my fax list history",
     *     operationId="get-a-history",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\Response(response="200",
     *         description="Fax list history found",
     *         @OA\JsonContent(
     *                           @OA\Property(
     *                              property="title",
     *                              type="string",
     *                              example="Fax created at 2020-07-27 10:15 with 3 page(s)"
     *                            ),
     *                           @OA\Property(
     *                              property="dst",
     *                              type="string",
     *                              example="+19091112233"
     *                            ),
     *                           @OA\Property(
     *                              property="created_at",
     *                              type="string",
     *                              example="2020-08-08 10:10:10"
     *                            ),
     *                           @OA\Property(
     *                              property="name",
     *                              type="string",
     *                              example="Documents for John"
     *                            ),
     *                           @OA\Property(
     *                              property="fax_id",
     *                              type="integer",
     *                              example=25
     *                            ),
     *                          @OA\Property(
     *                             property="sent_at",
     *                             type="timestamp",
     *                             example="2020-08-08 10:10:10"
     *                         ),
     *                          @OA\Property(
     *                             property="status",
     *                             type="string",
     *                             example="partial success"
     *                         ),
     *                      )
     *      )
     * )
     */

    /**
     * Show history of faxes
     */
    public function history()
    {
        $data = [];

        /** @var Fax[] $faxes */
        $faxes = Fax::query()
            ->whereUserId($this->guard()->user()->id)
            ->orderByDesc('id')
            ->get();

        foreach ($faxes as $fax) {
            $data[] = [
                'title' => $fax->dst . ' at ' . $fax->created_at->format('Y-m-d H:i') . ' with ' . count($fax->files) . ' page(s)',
                'dst' => $fax->dst,
                'created_at' => $fax->created_at->format('Y-m-d H:i:s'),
                'fax_id' => $fax->id,
                'sent_at' => $fax->sent_at ? $fax->sent_at->format('Y-m-d H:i:s') : null,
                'files' => $fax->files,
                'status' => $fax->getStatus(),
                'name' => $fax->name,
            ];
        }

        return response()->json($data);
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/fax/clean",
     *     tags={"fax"},
     *     summary="Clean fax history",
     *     description="Clean my fax list history",
     *     operationId="clean-a-history",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\Response(response="200",
     *         description="File successful uploaded to fax",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example=true
     *                 ),
     *             ),
     *         ),
     *      )
     * )
     */

    /**
     * Remove history of faxes
     * @return \Illuminate\Http\JsonResponse
     */
    public function cleanHistory()
    {
        Fax::whereUserId($this->guard()->user()->id)
            ->delete();

        return response()->json(['success' => true]);
    }
}
