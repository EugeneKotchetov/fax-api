<?php

namespace App\Http\Api\V1\Controllers;

use App\Models\Feedback;
use App\Http\Requests\FeedbackRequest;

class FeedbackController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/v1/feedback",
     *     tags={"feedback"},
     *     summary="Store an feedback",
     *     description="Store an feedback",
     *     operationId="StoreFeedback",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *              required={"text"},
     *                 @OA\Property(
     *                     property="rate",
     *                     type="integer",
     *                     example=5
     *                 ),
     *                 @OA\Property(
     *                     property="text",
     *                     type="string",
     *                     example="Text for rate"
     *                ),
     *          )
     *     ),
     *     @OA\Response(response="200",
     *         description="Create successful",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example="true"
     *                 ),
     *                 @OA\Property(
     *                      property="message",
     *                      type="string",
     *                      example="Feddback has been created"
     *                 ),
     *                 @OA\Property(
     *                      property="id",
     *                      type="integer",
     *                      example="1"
     *                 ),
     *             ),
     *         ),
     *     ),
     *     @OA\Response(response=422, description="Error: Unprocessable Entity")
     * )
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param FeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FeedbackRequest $request)
    {
        $request->merge(['user_id' => auth()->id()]);
        $feedback = Feedback::create($request->all());

        return response()->json([
            'success' => true,
            'message' => 'Feedback has been created',
            'id' => $feedback->id
        ]);
    }
}
