<?php

namespace App\Http\Api\V1\Controllers;

use App\Models\Tariff;
use App\Services\TariffService;
use Illuminate\Http\Request;

class TariffController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/tariffs",
     *     tags={"tariff"},
     *     summary="Get tariffs",
     *     description="Get tariffs",
     *     operationId="get-tariff",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\Response(response="200",
     *         description="Tariffs found",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(
     *                         property="id",
     *                         type="integer",
     *                         example=1
     *                     ),
     *                     @OA\Property(
     *                         property="title",
     *                         type="string",
     *                         example="Tariff title"
     *                     ),
     *                     @OA\Property(
     *                         property="apple_id",
     *                         type="string",
     *                         example="1234"
     *                     ),
     *                     @OA\Property(
     *                         property="description",
     *                         type="string",
     *                         example="Tariff description"
     *                     ),
     *                     @OA\Property(
     *                         property="price",
     *                         type="float",
     *                         example=100.5
     *                     ),
     *                     @OA\Property(
     *                         property="days",
     *                         type="integer",
     *                         example=30
     *                     ),
     *                     @OA\Property(
     *                         property="discount",
     *                         type="string",
     *                         example="Tariff discount"
     *                     ),
     *                     @OA\Property(
     *                         property="by_default",
     *                         type="bool",
     *                         example=true
     *                     ),
     *                     @OA\Property(
     *                         property="is_active",
     *                         type="bool",
     *                         example=true
     *                     ),
     *                     @OA\Property(
     *                         property="paid_until",
     *                         type="integer",
     *                         example=null
     *                     ),
     *                     @OA\Property(
     *                         property="group",
     *                         type="integer",
     *                         example=5
     *                     ),
     *                 ),
     *             ),
     *         ),
     *      )
     * )
     */

    public function list(TariffService $tariffService)
    {
        $user = $this->guard()->user();
        $tariffs = $tariffService->getList($user);

        return response()->json($tariffs);
    }
}
