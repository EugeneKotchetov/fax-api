<?php

namespace App\Http\Api\V1\Controllers\Auth;

use App\Models\User;
use App\Services\TariffService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Api\V1\Controllers\Controller;
use Tymon\JWTAuth\JWTGuard;
use App\Services\GeoIpService;

class AuthController extends Controller
{
    /**
     * @return JWTGuard
     */
    protected function guard(): Guard
    {
        $guard = parent::guard();

        if (!$guard instanceof JWTGuard) {
            throw new RuntimeException('Expected guard of type JWTGuard');
        }

        return $guard;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/login",
     *     tags={"user"},
     *     summary="Login user",
     *     description="Get a JWT via given credentials.",
     *     operationId="loginUser",
     *
     *    @OA\RequestBody(
     *         @OA\JsonContent(
     *              required={"username", "password"},
     *              @OA\Property(
     *                  property="username",
     *                  type="string",
     *                  example="user1"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *                  example="secret"
     *              ),
     *         )
     *     ),
     *
     *     @OA\Response(response="200",
     *         description="Login successful",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="token",
     *                      type="string",
     *                      example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1Ni..."
     *                 ),
     *                 @OA\Property(
     *                      property="screen",
     *                      type="integer",
     *                      example=1
     *                 ),
     *             ),
     *         ),
     *      ),
     *     @OA\Response(response="401",
     *         description="Unauthorized",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="error",
     *                      type="string",
     *                      example="Unauthorized"
     *                 ),
     *             ),
     *         ),
     *      )
     * )
     */

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['username', 'password']);

        if (! $token = $this->guard()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = $this->guard()->user();

        return response()->json([
            'token' => $token,
            'screen' => $user->screen,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/logout",
     *     tags={"user"},
     *     summary="Logout user",
     *     operationId="logoutUser",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\Response(response="200",
     *         description="Logout successful",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="message",
     *                      type="string",
     *                      example="Successfully logged out"
     *                 ),
     *             ),
     *         ),
     *      ),
     * )
     */

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return response()->json([
            'token' => $this->guard()->refresh(),
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/register",
     *     tags={"user"},
     *     summary="Register new user",
     *     description="Register new user.",
     *     operationId="RegisterUser",
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *              required={"username", "password", "os_version"},
     *              @OA\Property(
     *                  property="username",
     *                  type="string",
     *                  example="user1"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *                  example="secret"
     *              ),
     *              @OA\Property(
     *                  property="os_version",
     *                  type="string",
     *                  example="v1"
     *              )
     *         )
     *     ),
     *
     *     @OA\Response(response="200",
     *         description="Register successful",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="token",
     *                      type="string",
     *                      example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1Ni..."
     *                 ),
     *             ),
     *         ),
     *      ),
     *     @OA\Response(response=422, description="Error: Unprocessable Entity")
     * )
     */

    /**
     * Register new user
     *
     * @param Request $request
     * @param TariffService $tariffService
     * @param GeoIpService $geoIpService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request, TariffService $tariffService, GeoIpService $geoIpService)
    {
        $this->validate($request, [
            'username' => 'required|string|unique:users',
            'password' => 'required|string|min:8',
            'os_version' => 'required|string',
        ]);

        $tariffList = $tariffService->getAuction();
        if (!count($tariffList)) {
            abort(400, 'Tariffs is empty!');
        }

        $ip = $geoIpService->getIp();
        $geo = $geoIpService->getGeoByIp($ip);

        if (strtolower($geo['country']) == 'ps' || strtolower($geo['country']) == 'il') {
            return response()->json(['success' => false]);
        }

        $user =  User::create([
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'os_version' => $request->os_version,
            'ip' => $ip,
            'country' => $geo['country'],
            'city' => $geo['city'],
            'connection' => '',
            'proxy_type' => '',
            'is_active' => true,
            'tariff_list' => json_encode($tariffList),
            'screen' => rand(1,2),
        ]);

        $token = $this->guard()->login($user);

        return response()->json([
            'token' => $token,
            'screen' => $user->screen,
        ]);
    }

}
