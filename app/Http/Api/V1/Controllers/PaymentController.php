<?php

namespace App\Http\Api\V1\Controllers;

use App\Services\AppleService;
use App\Models\Tariff;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PaymentController
 *
 * https://developer.apple.com/documentation/storekit/in-app_purchase/validating_receipts_with_the_app_store
 * https://developer.apple.com/documentation/storekit/in-app_purchase/enabling_server-to-server_notifications
 *
 *
 * @package App\Http\Controllers
 */
class PaymentController extends Controller
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /** @var AppleService */
    private $appleService;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param AppleService $appleService
     *
     * @return void
     */
    public function __construct(Request $request, AppleService $appleService)
    {
        $this->appleService = $appleService;
        $this->request = $request;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/payment",
     *     tags={"payment"},
     *     summary="Make an App-in payment",
     *     description="Make an App-in payment",
     *     operationId="payment",
     *     security={
     *         {"bearer": {}}
     *     },
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *              required={"receipt", "tariff_id"},
     *              @OA\Property(
     *                  property="receipt",
     *                  type="string",
     *                  description="Apple Receipt base64 encoded",
     *              ),
     *              @OA\Property(
     *                  property="tariff_id",
     *                  type="integer",
     *              ),
     *         )
     *     ),
     *
     *     @OA\Response(response="200",
     *         description="Payment successful",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example="true"
     *                 ),
     *                 @OA\Property(
     *                      property="data",
     *                      type="object",
     *                      example=""
     *                 ),
     *             ),
     *         ),
     *      )
     * )
     */

    /**
     * Payment control, save transation, verify from AppleStore/Market
     *
     * @var AppleService $appleService
     * @return mixed
     */
    public function index()
    {

        $this->validate($this->request, [
            'receipt'  => 'required',
            'tariff_id' => 'required',
        ]);

        $receipt = $this->request->input('receipt');
        $store   = $this->request->input('store');
        $tariffId = $this->request->input('tariff_id');

        $tariff = Tariff::where('id', $tariffId)->first();
        if (!$tariff) {
            throw new NotFoundHttpException('Tariff not found');
        }

        $user = $this->guard()->user();

        if ($store == 'playmarket') {

            //todo: validation of subscription

            // save transaction
            $transaction = new Transaction();
            $transaction->type = $this->request->input('type', Transaction::TYPE_ORIGINAL);
            $transactionStatus = $transaction->saveFromGoogleResponse($receipt, $user->id);

            $tariff->is_active = true;

            $user = $this->setUserTime($user, $tariff);

            //just for output
            $tariff->paid_until = $user->paid_until;

            return response()->json([
                'success' => true,
                'data' => $tariff,
            ], 200);

        } else {

            $verification = $this->appleService->verifyReceipt($receipt, $this->request->input('is_test', false));

            if ($verification->status == 0) {

                // save transaction
                $transaction = new Transaction();
                $transaction->type = $this->request->input('type', Transaction::TYPE_ORIGINAL);
                $transactionStatus = $transaction->saveFromAppleResponse($verification, $user->id, $receipt);

                // if successful transaction - add days
                if ($transactionStatus) {
                    $tariff->is_active = true;
                }

                $user = $this->setUserTime($user, $tariff);

                //just for output
                $tariff->paid_until = $user->paid_until;

                return response()->json([
                    'success' => true,
                    'data' => $tariff,
                ], 200);
            }

            Log::error(json_encode($verification));

            // Bad Request response
            return response()->json([
                'success' => false,
                'response' => json_encode($verification),
            ], 400);

        }
    }

    private function setUserTime($user, Tariff $tariff)
    {
        $days = $tariff->days;

        $user->tariff_id = $tariff->id;
        $user->paid_until = strtotime("+" . $days ." days");
        $user->payment_amount = $tariff->price;
        $user->save();

        return $user;
    }

    /**
     * Callback from Apple about subscription
     */
    public function prolongation()
    {
        $this->validate($this->request, [
            'latest_receipt'  => 'required',
        ]);

        Log::error('Status: ' . json_encode($this->request->input('notification_type')));
        Log::error(json_encode($this->request->input('latest_receipt')));
        Log::error(json_encode($this->request->input('latest_receipt_info')));

        $notificationType = $this->request->input('notification_type');

//        if ($notificationType== 'RENEWAL' || $notificationType == 'INTERACTIVE_RENEWAL') {
//            $data = $this->request->input('latest_receipt_info');
//
//            if (isset($data->original_transaction_id)) {
//
//                $orinTransaction = Transaction::where('original_transaction_id', $data->original_transaction_id)->first();
//
//                if ($orinTransaction) {
//
//                    $tariff = Tariff::where('apple_id', $data->product_id)->first();
//                    if (!$tariff) {
//                        throw new NotFoundHttpException('Tariff not found');
//                    }
//
//                    $user = $orinTransaction->user;
//                    $user->tariff_id = $tariff->id;
//                    $user->paid_until = strtotime("+" . $tariff->days ." days");
//                    $user->save();
//
//                    $newTransaction = new Transaction();
//                    $newTransaction->saveFromAppleResponse($this->request, $user->id, json_encode($this->request));
//                }
//
//            }
//        }

        if ($notificationType == 'DID_CHANGE_RENEWAL_STATUS' || $notificationType == 'INTERACTIVE_RENEWAL') {

            $verification = $this->appleService->verifyReceipt($this->request->input('latest_receipt'));

            // auto_renew_status = 0 значит отмену
            if (isset($verification->auto_renew_status) && $verification->auto_renew_status === 0) {

                /** @var Transaction $orinTransaction */
                $orinTransaction = Transaction::where('original_transaction_id', $verification->receipt->original_transaction_id)->first();
                if ($orinTransaction) {
                    $orinTransaction->cancel();
                    $orinTransaction->save();

                    $user = $orinTransaction->user;
                    $user->tariff_id = 0;
                    $user->save();
                }
            }
        }
    }

}
