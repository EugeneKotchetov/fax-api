<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class SwaggerController extends Controller
{
    /**
     * @OA\Info(
     *     description="This is a sample Template API server.",
     *     version="1.0.0",
     *     title="Template API",
     *     @OA\Contact(
     *         email="apiteam@swagger.io"
     *     )
     * )
     *
     * @OA\ExternalDocumentation(
     *     description="External documentation: JWT",
     *     url="https://jwt.io/"
     * )
     *
     */

    /**
     * @OA\Server(
     *      url="{schema}://template.loc",
     *      description="OpenApi parameters",
     *      @OA\ServerVariable(
     *          serverVariable="schema",
     *          enum={"https", "http"},
     *          default="http"
     *      )
     * )
     */

    /**
     * @OA\SecurityScheme(
     *     type="http",
     *     description="Authorization use JWT token",
     *     scheme="bearer",
     *     securityScheme="bearer",
     *     bearerFormat="JWT",
     *     @OA\Get(
     *          path="/api/v1/auth",
     *           @OA\Response(
     *              response="200",
     *              description="An example resource")
     *      )
     * )
     *
     *
     * @OA\Tag(
     *     name="user",
     *     description="Operations about user",
     *     @OA\ExternalDocumentation(
     *         description="API user operations",
     *         url="http://swagger.io"
     *     )
     * )
     *
     * @OA\Tag(
     *     name="feedback",
     *     description="Operations about feedback",
     *     @OA\ExternalDocumentation(
     *         description="API feedback operations",
     *         url="http://swagger.io"
     *     )
     * )
     *
     */




    public function getYaml()
    {
        require("../vendor/autoload.php");
        $openapi = \OpenApi\scan('../app');
        header('Content-Type: application/x-yaml');
        echo $openapi->toYaml();
    }

    public function index()
    {
        return view('swagger.index');
    }

    public function redoc()
    {
        return view('swagger.redoc');
    }
}
