<?php

namespace App\Http\Admin\Controllers;

use App\Models\Feedback;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the feedbacks
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'feedbacks' => Feedback::with(['user'])->orderByDesc('id')->paginate()
        ];
        return view('feedback.index', $data);
    }

}
