<?php

namespace App\Http\Admin\Controllers;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RebillController extends Controller
{
    /**
     * get View of statistics page
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        //Timezone- default 'UTC'
        $timeZone = isset($request->timezone) ? $request->timezone : 'UTC';
        DB::select(DB::raw("SET SESSION time_zone = '".$timeZone."'"));

        $daysArr = DB::table('tariffs')
            ->selectRaw("days")
            ->where('is_active', true)
            ->groupBy('days')
            ->pluck('days')
            ->toArray();


        $firstTransactionDate = Transaction::orderBy('created_at')->first()->created_at->toDateString();
        $tomorrowDate = Carbon::tomorrow()->toDateString();

        $result = collect();
        $totalCount = 0;
        $totalAmount = 0;

        foreach ($daysArr as $days) {
            $count = 0;
            $amount = 0;
            $calcDate = $tomorrowDate;

            while (Carbon::parse($calcDate)->isBetween($firstTransactionDate, $tomorrowDate)) {
                $calcDate = Carbon::parse($calcDate)->subDays($days)->toDateString();

                $transactions = DB::table('transactions')
                    ->selectRaw("COUNT(transactions.id) count, SUM(tariffs.price) amount, tariffs.days")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->where('transactions.is_cancelled', '<>', true)
                    ->where('tariffs.days', '=', $days)
                    ->where('tariffs.is_active', '=', true)
                    ->whereDate('transactions.created_at', '=', $calcDate)
                    ->groupBy('tariffs.days')
                    ->first();

                $count += isset($transactions->count) ? $transactions->count : 0;
                $amount += isset($transactions->amount) ? $transactions->amount : 0;
            }

            $totalCount += $count;
            $totalAmount += $amount;

            $result->push([
                'days' =>  $days,
                'transactions' => $count,
                'amount' => $amount,
            ]);
        }

        $data = [
            'rebills' =>  $result->sortBy('days')->toArray(),
            'totalCount' => $totalCount,
            'totalAmount' => $totalAmount,
            'timeZone' => $timeZone,
        ];

        //Restore app timezone
        DB::select(DB::raw("SET SESSION time_zone = '".config('app.timezone')."'"));

        return view('rebills.index', $data);
    }


}
