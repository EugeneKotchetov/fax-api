<?php

namespace App\Http\Admin\Controllers;

use App\Models\Tariff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Transaction;
use Illuminate\Support\Facades\Cookie;

class StatisticController extends Controller
{
    /**
     * get View of statistics page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $fields = Cookie::get('fields');
        if (!isset($fields)) {
            $fields = json_encode($this->getDefaultFields());
            Cookie::queue('fields', $fields, 60*24*365);
        }

        $totalActive = Transaction::notCancelled()->count();
        $groups = Tariff::query()->select('group')
            ->where('is_active', true)
            ->distinct()->pluck('group');

        $data = [
            'fields' => json_decode($fields),
            'fieldNames' => $this->getFields(),
            'totalActive' => $totalActive,
            'groups' => $groups,
        ];

        return view('statistics.index', $data);
    }

    /**
     * get HTML table of the statistics group
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function fetch(Request $request)
    {
        $data = $this->getData($request);

        return view('statistics.table', $data);
    }

    /**
     * Data to CSV download
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function toCsv(Request $request)
    {
        $data = $this->getData($request);
        $fields = $data['fields'];
        $statistics = $data['statistics'];

        $output = 'Group;';
        $output .= isset($fields->users) ? 'Users;' : '';
        $output .= isset($fields->transactions) ? 'Transactions;' : '';
        $output .= isset($fields->transactions_late) ? 'Transactions late;' : '';
        $output .= isset($fields->transactions_amount) ? 'TS Amount;' : '';
        $output .= isset($fields->cr) ? 'CR;' : '';
        $output .= isset($fields->cancellations) ? 'Cancellations;' : '';
        $output .= isset($fields->cancellations_late) ? 'Cancellations late;' : '';
        $output .= isset($fields->active) ? 'Active;' : '';
        $output .= isset($fields->rebills) ? 'Rebills;' : '';
        $output .= isset($fields->rebills_amount) ? 'RB Amount;' : '';
        $output .= PHP_EOL;

        foreach ($statistics as $statistic) {
            $st = $statistic['name'] . ';';
            $st .= isset($fields->users) ? $statistic['users'] . ';' : '';
            $st .= isset($fields->transactions) ? $statistic['transactions'] . ';' : '';
            $st .= isset($fields->transactions_late) ? $statistic['transactionsLate'] . ';' : '';
            $st .= isset($fields->transactions_amount) ? '$' . $statistic['transactionsAmount'] . ';' : '';
            $st .= isset($fields->cr) ? $statistic['ctr'] . '%;' : '';
            $st .= isset($fields->cancellations) ? $statistic['cancelled'] . ';' : '';
            $st .= isset($fields->cancellations_late) ? $statistic['cancelledLate'] . ';' : '';
            $st .= isset($fields->active) ? $statistic['active'] . ';' : '';
            $st .= isset($fields->rebills) ? $statistic['renewed']  . ';': '';
            $st .= isset($fields->rebills_amount) ? '$' . $statistic['renewedAmount'] . ';' : '';
            $st .= PHP_EOL;

            $output .= $st;
        }

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="Statistics_' .
                Carbon::parse($data['daterange'][0])->format('Y-m-d') . '_' .
                Carbon::parse($data['daterange'][1])->format('Y-m-d') . '.csv"',
        ];

        return response(rtrim($output, "\n"), 200, $headers);
    }

    private function applyCommission($fields, $value)
    {
        if (isset($fields->amount70)) {
            return round($value * 0.7, 2);
        } else if (isset($fields->amount85)) {
            return round($value * 0.85, 2);
        } else {
            return $value;
        }
    }

    /**
     * get statistics data
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    private function getData(Request $request): array
    {
        $fields = $request->fields;
        Cookie::queue('fields', $fields, 60*24*365);
        $fields = json_decode($fields);

        //Timezone- default 'Europe/Moscow'
        $timeZone = isset($request->timezone) ? $request->timezone : 'Europe/Moscow';
        DB::select(DB::raw("SET SESSION time_zone = '".$timeZone."'"));

        //Today by default or when request is empty
        $dateRange = [Carbon::now()->format('Y-m-d'), Carbon::now()->format('Y-m-d')];

        if (isset($request->daterange)) {
            $dateRange = json_decode($request->daterange);
        }

        $dateStart = date($dateRange[0] . ' 00:00:00');
        $dateEnd= date($dateRange[1] . ' 23:59:59');

        //Date type- default 'transaction'
        $dateType = $request->input('datetype', 'transaction');

        //Group by - default 'day'
        $group = $request->input('group', 'day');

        $filterByGroup = $request->input('groupFilter');

        if ($group == 'day') {
            $groupFormat = '%Y-%m-%d';
        } else if ($group == 'month') {
            $groupFormat = '%Y-%m';
        } else {
            $groupFormat = '%H';
        }

        $cancelledDateType = 'transactions.created_at';
        $renewedDateType = 'transactions.purchase_date';

        if ($dateType == 'action') {
            $cancelledDateType = 'transactions.cancelled_date';
            $renewedDateType = 'transactions.renewed_date';
        }

        if ($group == 'day' || $group == 'hour' || $group == 'month') {
            $transactions = DB::table('transactions')
                ->selectRaw("COUNT(*) transactions, DATE_FORMAT(transactions.created_at, '".$groupFormat."') name")
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $transactionsLate = DB::table('transactions')
                ->selectRaw("COUNT(*) ts_late, DATE_FORMAT(transactions.created_at, '".$groupFormat."') name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereRaw('DATE(users.created_at) <> DATE(transactions.purchase_date)')
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $transactionsAmount = DB::table('transactions')
                ->selectRaw("SUM(tariffs.price) ts_amount, DATE_FORMAT(transactions.created_at, '".$groupFormat."') name")
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $cancelled = DB::table('transactions')
                ->selectRaw("COUNT(cancelled_date) cancelled, DATE_FORMAT(".$cancelledDateType.", '".$groupFormat."') name")
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween($cancelledDateType, [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $cancelledLate = DB::table('transactions')
                ->selectRaw("COUNT(cancelled_date) cancelled_late, DATE_FORMAT(".$cancelledDateType.", '".$groupFormat."') name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereRaw('DATE(users.created_at) <> DATE('.$cancelledDateType.')')
                ->whereBetween($cancelledDateType, [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            if ($dateType == 'action') {
                $renewedDateType = 'transactions.purchase_date';
                $renewed = DB::table('transactions')
                    ->selectRaw("COUNT(*) renewed, DATE_FORMAT(".$renewedDateType.", '".$groupFormat."') name")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_RE_BILL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();

                $renewedAmount = DB::table('transactions')
                    ->selectRaw("SUM(tariffs.price) rb_amount, DATE_FORMAT(".$renewedDateType.", '".$groupFormat."') name")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_RE_BILL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();
            } else {
                $renewed = DB::table('transactions')
                    ->selectRaw("SUM(transactions.rebills_count) renewed, DATE_FORMAT(".$renewedDateType.", '".$groupFormat."') name")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();

                $renewedAmount = DB::table('transactions')
                    ->selectRaw("SUM(transactions.rebills_count * tariffs.price) rb_amount, DATE_FORMAT(".$renewedDateType.", '".$groupFormat."') name")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->where('transactions.is_renewed', 1)
                    ->groupBy('name')
                    ->get();
            }

            $users = DB::table('users')
                ->selectRaw("COUNT(*) users, DATE_FORMAT(created_at, '".$groupFormat."') name")
                ->whereBetween('created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();
        }

        if ($group == 'country' || $group == 'city' || $group == 'os_version'  || $group == 'version'  || $group == 'screen') {
            $transactions = DB::table('transactions')
                ->selectRaw("COUNT(*) transactions, users." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $transactionsLate = DB::table('transactions')
                ->selectRaw("COUNT(*) transactions, users." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->whereRaw('DATE(users.created_at) <> DATE(transactions.purchase_date)')
                ->groupBy('name')
                ->get();

            $transactionsAmount = DB::table('transactions')
                ->selectRaw("SUM(tariffs.price) ts_amount, users." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $cancelled = DB::table('transactions')
                ->selectRaw("COUNT(cancelled_date) cancelled, users." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween($cancelledDateType, [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $cancelledLate = DB::table('transactions')
                ->selectRaw("COUNT(cancelled_date) cancelled, users." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereRaw('DATE(users.created_at) <> DATE('.$cancelledDateType.')')
                ->whereBetween($cancelledDateType, [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            if ($dateType == 'action') {
                $renewedDateType = 'transactions.purchase_date';
                $renewed = DB::table('transactions')
                    ->selectRaw("COUNT(*) renewed, users." . $group . " name")
                    ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_RE_BILL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();

                $renewedAmount = DB::table('transactions')
                    ->selectRaw("SUM(tariffs.price) rb_amount, users." . $group . " name")
                    ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_RE_BILL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();
            } else {
                $renewed = DB::table('transactions')
                    ->selectRaw("SUM(transactions.rebills_count) renewed, users." . $group . " name")
                    ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();

                $renewedAmount = DB::table('transactions')
                    ->selectRaw("SUM(transactions.rebills_count * tariffs.price) rb_amount, users." . $group . " name")
                    ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->where('transactions.is_renewed', 1)
                    ->groupBy('name')
                    ->get();
            }

            $users = DB::table('users')
                ->selectRaw("COUNT(*) users, " . $group . " as name")
                ->whereBetween('created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();
        }

        if ($group == 'apple_id' || $group == 'group') {
            $transactions = DB::table('transactions')
                ->selectRaw("COUNT(*) transactions, tariffs." . $group . " name")
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $transactionsLate = DB::table('transactions')
                ->selectRaw("COUNT(*) transactions, tariffs." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->whereRaw('DATE(users.created_at) <> DATE(transactions.purchase_date)')
                ->groupBy('name')
                ->get();

            $transactionsAmount = DB::table('transactions')
                ->selectRaw("SUM(tariffs.price) ts_amount, tariffs." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $cancelled = DB::table('transactions')
                ->selectRaw("COUNT(cancelled_date) cancelled, tariffs." . $group . " name")
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween($cancelledDateType, [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $cancelledLate = DB::table('transactions')
                ->selectRaw("COUNT(cancelled_date) cancelled, tariffs." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereRaw('DATE(users.created_at) <> DATE('.$cancelledDateType.')')
                ->whereBetween($cancelledDateType, [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            if ($dateType == 'action') {
                $renewedDateType = 'transactions.purchase_date';
                $renewed = DB::table('transactions')
                    ->selectRaw("COUNT(*) renewed, tariffs." . $group . " name")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_RE_BILL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();

                $renewedAmount = DB::table('transactions')
                    ->selectRaw("SUM(tariffs.price) rb_amount, tariffs." . $group . " name")
                    ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_RE_BILL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();
            } else {
                $renewed = DB::table('transactions')
                    ->selectRaw("SUM(transactions.rebills_count) renewed, tariffs." . $group . " name")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();

                $renewedAmount = DB::table('transactions')
                    ->selectRaw("SUM(transactions.rebills_count * tariffs.price) rb_amount, tariffs." . $group . " name")
                    ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->where('transactions.is_renewed', 1)
                    ->groupBy('name')
                    ->get();
            }

            $users = DB::table('users');

            if ($group == 'group') {
                $users = $users->selectRaw("COUNT(*) users, (select `group` from tariffs where id = JSON_EXTRACT(tariff_list, '$[0]')) name");
            } else {
                $users = $users->selectRaw("COUNT(*) users, apple_id name")
                    ->join('tariffs', function ($join) {
                        $join->on(function ($on) {
                            $on->whereRaw("JSON_CONTAINS(tariff_list, CAST(tariffs.id as JSON), '$')");
                        });
                    });
            }

            $users = $users->whereBetween('users.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();
        }

        if ($group == 'store') {
            $transactions = DB::table('transactions')
                ->selectRaw("COUNT(*) transactions, transactions." . $group . " name")
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $transactionsLate = DB::table('transactions')
                ->selectRaw("COUNT(*) transactions, transactions." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->whereRaw('DATE(users.created_at) <> DATE(transactions.purchase_date)')
                ->groupBy('name')
                ->get();

            $transactionsAmount = DB::table('transactions')
                ->selectRaw("SUM(tariffs.price) ts_amount, transactions." . $group . " name")
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $cancelled = DB::table('transactions')
                ->selectRaw("COUNT(cancelled_date) cancelled, transactions." . $group . " name")
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereBetween($cancelledDateType, [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            $cancelledLate = DB::table('transactions')
                ->selectRaw("COUNT(cancelled_date) cancelled, transactions." . $group . " name")
                ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                ->whereRaw('DATE(users.created_at) <> DATE('.$cancelledDateType.')')
                ->whereBetween($cancelledDateType, [$dateStart, $dateEnd])
                ->groupBy('name')
                ->get();

            if ($dateType == 'action') {
                $renewedDateType = 'transactions.purchase_date';
                $renewed = DB::table('transactions')
                    ->selectRaw("COUNT(*) renewed, transactions." . $group . " name")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_RE_BILL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();

                $renewedAmount = DB::table('transactions')
                    ->selectRaw("SUM(tariffs.price) rb_amount, transactions." . $group . " name")
                    ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_RE_BILL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();
            } else {
                $renewed = DB::table('transactions')
                    ->selectRaw("SUM(transactions.rebills_count) renewed, transactions." . $group . " name")
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->groupBy('name')
                    ->get();

                $renewedAmount = DB::table('transactions')
                    ->selectRaw("SUM(transactions.rebills_count * tariffs.price) rb_amount, transactions." . $group . " name")
                    ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
                    ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
                    ->where('transactions.type', Transaction::TYPE_ORIGINAL)
                    ->whereBetween($renewedDateType, [$dateStart, $dateEnd])
                    ->where('transactions.is_renewed', 1)
                    ->groupBy('name')
                    ->get();
            }

            $users = collect();
        }

        $statistics = $users
            ->merge($transactions)
            ->merge($transactionsLate)
            ->merge($transactionsAmount)
            ->merge($cancelled)
            ->merge($cancelledLate)
            ->merge($renewed)
            ->merge($renewedAmount)
            ->groupBy('name');

        $result = collect();

        $count = (object) [
            'users' => 0,
            'transactions' => 0,
            'transactionsAmount' => 0,
            'transactionsLate' => 0,
            'ctr' => 0,
            'cancelled' => 0,
            'cancelledLate' => 0,
            'renewed' => 0,
            'renewedAmount' => 0,
            'active' => 0,
        ];

        $tariffs = Tariff::all();

        foreach ($statistics as $statistic) {
            $name = $statistic->pluck('name')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();

            $name = $name->count() ? $name[0] : 0;

            if ($group == 'hour') {
                $hour = (int)$name;
                if ($hour >= 24) {
                    $hour = $hour - 24;
                }
                $to_hour = $hour + 1;
                $name = ($hour >= 10 ? $hour : '0' . $hour) . ':00 - ' . ($to_hour >= 10 ? $to_hour : '0' . $to_hour) . ':00' ;
            }

            if ($group == 'apple_id') {
                $tariff = $tariffs->where('apple_id', '=', $name);

                $name = 'Group ' . $tariff->pluck('group')[0] . ' - ';
                $name .= $tariff->pluck('price')[0];
            }

            $users = $statistic->pluck('users')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();
            $users = $users->count() ? $users[0] : 0;
            $count->users += $users;

            $transactions = $statistic->pluck('transactions')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();
            $transactions = $transactions->count() ? $transactions[0] : 0;
            $count->transactions += $transactions;

            $transactionsLate = $statistic->pluck('ts_late')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();
            $transactionsLate = $transactionsLate->count() ? $transactionsLate[0] : 0;
            $count->transactionsLate += $transactionsLate;

            $transactionsAmount = $statistic->pluck('ts_amount')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();
            $transactionsAmount = $transactionsAmount->count() ? $transactionsAmount[0] : 0;
            $count->transactionsAmount += $transactionsAmount;

            $cancelled = $statistic->pluck('cancelled')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();
            $cancelled = $cancelled->count() ? $cancelled[0] : 0;
            $count->cancelled += $cancelled;

            $cancelledLate = $statistic->pluck('cancelled_late')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();
            $cancelledLate = $cancelledLate->count() ? $cancelledLate[0] : 0;
            $count->cancelledLate += $cancelledLate;

            $renewed = $statistic->pluck('renewed')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();
            $renewed = $renewed->count() ? $renewed[0] : 0;
            $count->renewed += $renewed;

            $renewedAmount = $statistic->pluck('rb_amount')->filter(function ($item) {
                if (!is_null($item)) return $item;
            })->values();
            $renewedAmount = $renewedAmount->count() ? $renewedAmount[0] : 0;
            $count->renewedAmount += $renewedAmount;

            $ctr = $users != 0 ? $transactions / $users * 100 : 0;

            $result->push([
                'name' => $name,
                'users' => $users,
                'transactions' => $transactions,
                'transactionsLate' => $transactionsLate,
                'transactionsAmount' => $this->applyCommission($fields, $transactionsAmount),
                'cancelled' => $cancelled,
                'cancelledLate' => $cancelledLate,
                'renewed' => $renewed,
                'renewedAmount' => $this->applyCommission($fields, $renewedAmount),
                'ctr' => round($ctr, 2),
                'active' => $transactions - $cancelled,
            ]);
        }

        $count->ctr = $count->users != 0 ? (round($count->transactions / $count->users * 100, 2)) : 0;
        $count->active = $count->transactions - $count->cancelled;
        $count->transactionsAmount = $this->applyCommission($fields, $count->transactionsAmount);
        $count->renewedAmount = $this->applyCommission($fields, $count->renewedAmount);

        //Restore app timezone
        DB::select(DB::raw("SET SESSION time_zone = '".config('app.timezone')."'"));

        return [
            'daterange' => $dateRange,
            'datetype' => $dateType,
            'group' => $group,
            'statistics' => $result->sortBy('name')->toArray(),
            'count' => $count,
            'fields' => $fields,
        ];
    }

    private function getFields(): array
    {
        return [
            'users' => 'Users',
            'transactions' => 'Transactions',
            'transactions_late' => 'Transactions Late',
            'transactions_amount' => 'TS Amount',
            'cr' => 'CR',
            'cancellations' => 'Cancellations',
            'cancellations_late' => 'Cancellations Late',
            'active' => 'Active',
            'rebills' => 'Rebills',
            'rebills_amount' => 'RB Amount',
            'amount70' => 'Amount - 30%',
            'amount85' => 'Amount - 15%'
        ];
    }

    private function getDefaultFields(): array
    {
        return [
            'users' => 1,
            'transactions' => 1,
            'transactions_late' => 1,
            'transactions_amount' => 1,
            'cr' => 1,
            'cancellations' => 1,
            'cancellations_late' => 1,
            'active' => 1,
            'rebills' => 1,
            'rebills_amount' => 1,
            'amount70' => 0,
            'amount85' => 0,
        ];
    }
}
