<?php

namespace App\Http\Admin\Controllers;

use App\Models\Admin;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\AdminPasswordRequest;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the admins
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'admins' => Admin::paginate(),
        ];
        return view('admins.index', $data);
    }

    /**
     * Show the form for creating admin profile.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admins.create');
    }

    /**
     * Show the form for editing admin profile.
     *
     * @param Admin $admin
     * @return \Illuminate\View\View
     */
    public function edit(Admin $admin)
    {
        $data = [
            'admin' => $admin,
        ];
        return view('admins.edit', $data);
    }

    /**
     * Store admin profile
     *
     * @param AdminRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminRequest $request)
    {

        Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return back()->withStatus(__('Admin profile successfully created.'));
    }

    /**
     * Update admin profile
     *
     * @param AdminRequest $request
     * @param Admin $admin
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminRequest $request, Admin $admin)
    {
        $admin->update($request->all());

        return back()->withStatus(__('Admin profile successfully updated.'));
    }

    /**
     * Destroy admin profile
     *
     * @param Admin $admin
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Admin $admin)
    {
        if(auth()->id() == $admin->id) {
            return back()->withError(__('Admin can not delete himself.'));
        }

        $admin->delete();

        return back()->withStatus(__('Admin successfully deleted.'));
    }

    /**
     * Change the password
     *
     * @param AdminPasswordRequest $request
     * @param Admin $admin
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(AdminPasswordRequest $request, Admin $admin)
    {
        $admin->update(['password' => Hash::make($request->get('password'))]);

        return back()->withPasswordStatus(__('Password successfully updated.'));
    }
}
