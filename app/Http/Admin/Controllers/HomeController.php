<?php

namespace App\Http\Admin\Controllers;

use App\Services\DashboardService;
use Carbon\Carbon;

class HomeController extends Controller
{
    /** @var DashboardService */
    private $dashboardService;

    /**
     * Create a new controller instance.
     *
     * @param DashboardService $dashboardService
     */
    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $usersTodayData = $this->dashboardService->getUsersChartData([
            Carbon::today(), Carbon::today(),
        ]);

        $usersYesterdayData = $this->dashboardService->getUsersChartData([
            Carbon::yesterday(), Carbon::yesterday(),
        ]);

        $usersTodayTrend = $usersYesterdayData['total'] != 0 ?
            $usersTodayData['total'] * 100 / $usersYesterdayData['total'] - 100 : 0;

        $usersMonthData = $this->dashboardService->getUsersChartData([
            Carbon::today()->subMonth()->addDay(), Carbon::today(),
        ]);

        $usersWeekData = $this->dashboardService->getUsersChartData([
            Carbon::today()->subWeek()->addDay(), Carbon::today(),
        ]);

        $usersLastWeekData = $this->dashboardService->getUsersChartData([
            Carbon::today()->subWeek(2)->addDay(), Carbon::today()->subWeek()->addDay(),
        ]);

        $usersWeekTrend = $usersLastWeekData['total'] != 0 ?
            $usersWeekData['total'] * 100 / $usersLastWeekData['total'] - 100 : 0;

        $transactionsTodayData = $this->dashboardService->getTransactionsChartData([
            Carbon::today(), Carbon::today(),
        ]);

        $transactionsYesterdayData = $this->dashboardService->getTransactionsChartData([
            Carbon::yesterday(), Carbon::yesterday(),
        ]);

        $transactionsTodayTrend = $transactionsYesterdayData['total'] != 0 ?
            $transactionsTodayData['total'] * 100 / $transactionsYesterdayData['total'] - 100 : 0;

        $transactionsWeekData = $this->dashboardService->getTransactionsChartData([
            Carbon::today()->subWeek()->addDay(), Carbon::today(),
        ]);

        $transactionsLastWeekData = $this->dashboardService->getTransactionsChartData([
            Carbon::today()->subWeek(2)->addDay(), Carbon::today()->subWeek()->addDay(),
        ]);

        $transactionsWeekTrend = $transactionsLastWeekData['total'] != 0 ?
            $transactionsWeekData['total'] * 100 / $transactionsLastWeekData['total'] - 100 : 0;

        $rebillsTodayData = $this->dashboardService->getRebillsData([
            Carbon::today(), Carbon::today(),
        ]);

        $rebillsYesterdayData = $this->dashboardService->getRebillsData([
            Carbon::yesterday(), Carbon::yesterday(),
        ]);

        $rebillsTodayTrend = $rebillsYesterdayData['total'] != 0 ?
            $rebillsTodayData['total'] * 100 / $rebillsYesterdayData['total'] - 100 : 0;

        $rebillsWeekData = $this->dashboardService->getRebillsData([
            Carbon::today()->subWeek()->addDay(), Carbon::today(),
        ]);

        $rebillsLastWeekData = $this->dashboardService->getRebillsData([
            Carbon::today()->subWeek(2)->addDay(), Carbon::today()->subWeek()->addDay(),
        ]);

        $rebillsWeekTrend = $rebillsLastWeekData['total'] != 0 ?
            $rebillsWeekData['total'] * 100 / $rebillsLastWeekData['total'] - 100 : 0;

        $data = [
            'usersMonthChart' => json_encode($usersMonthData['chat']),
            'usersWeekChart' => json_encode($usersWeekData['chat']),
            'transactionsWeekChart' => json_encode($transactionsWeekData['chat']),
            'usersTodayTotal' => $usersTodayData['total'],
            'usersWeekTotal' => $usersWeekData['total'],
            'transactionsTodayTotal' => $transactionsTodayData['total'],
            'transactionsWeekTotal' => $transactionsWeekData['total'],
            'rebillsTodayTotal' => $rebillsTodayData['total'],
            'rebillsWeekTotal' => $rebillsWeekData['total'],
            'usersTodayTrend' => (int) $usersTodayTrend,
            'usersWeekTrend' => (int) $usersWeekTrend,
            'transactionsTodayTrend' => (int) $transactionsTodayTrend,
            'transactionsWeekTrend' => (int) $transactionsWeekTrend,
            'rebillsTodayTrend' => (int) $rebillsTodayTrend,
            'rebillsWeekTrend' => (int) $rebillsWeekTrend,
        ];

        return view('dashboard', $data);
    }

}
