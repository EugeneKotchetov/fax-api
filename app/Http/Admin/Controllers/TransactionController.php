<?php

namespace App\Http\Admin\Controllers;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TransactionController extends Controller
{
    /**
     * Display a listing of the transactions
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = $this->getData($request, true);

        return view('transactions.index', $data);
    }

    /**
     * Data to CSV download
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function toCsv(Request $request)
    {
        $data = $this->getData($request, false);

        $output = 'Id; User Id; Store; User created date; Country; Transaction_id; Amount; Purchase date; Renew date; Re-bills count; Cancellation date; Days Live' . PHP_EOL;

        foreach ($data['transactions'] as $transaction) {
            $st = $transaction->id . ';';
            $st .= $transaction->user ? $transaction->user->id . ';' : ';';
            $st .= $transaction->store . ';';
            $st .= $transaction->user ? $transaction->user->created_at . ';' : ';';
            $st .= $transaction->user ? $transaction->user->country . ';' : '-;';
            $st .= $transaction->transaction_id == 1 ? $transaction->order_id . ';' : $transaction->transaction_id . ';';
            $st .= $transaction->tariff ? '$' . $transaction->tariff->price . ';' : ';';
            $st .= $transaction->purchase_date . ';';
            $st .= $transaction->is_renewed ? $transaction->renewed_date . ';' : ';';
            $st .= $transaction->rebills_count . ';';
            $st .= $transaction->is_cancelled ? $transaction->cancelled_date . ';' : ';';
            if ($transaction->purchase_date) {
                if ($transaction->is_cancelled) {
                    $st .= $transaction->purchase_date->diffInDays($transaction->cancelled_date) . ';';
                } else {
                    $st .= $transaction->purchase_date->diffInDays(Carbon::now()) . ';';
                }
            } else {
                $st .= ';';
            }
            $output .= $st . PHP_EOL;
        }

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="Transactions_' .
                Carbon::parse($data['daterange'][0])->format('Y-m-d') . '_' .
                Carbon::parse($data['daterange'][1])->format('Y-m-d') . '.csv"',
        ];

        return response(rtrim($output, "\n"), 200, $headers);
    }

    /**
     * Get transactions data
     *
     * @param Request $request
     * @param bool $needPaginate
     * @return array
     */
    protected function getData(Request $request, bool $needPaginate): array
    {
        //Today by default or when request is empty
        $dateRange = [Carbon::now()->format('Y-m-d'), Carbon::now()->format('Y-m-d')];

        if (isset($request->daterange)) {
            $dateRange = json_decode($request->daterange);
        }

        $dateStart = date($dateRange[0] . ' 00:00:00');
        $dateEnd = date($dateRange[1] . ' 23:59:59');

        //Show only active transactions - default false
        $showActive = false;
        if (isset($request->showactive) && $request->showactive === 'true') {
            $showActive = true;
        }

        $storeTypes = ['AS', 'PM'];

        //Show only Apple Store (AS) transactions - default true
        $showAppleStore = true;
        if (isset($request->showas) && $request->showas === 'false') {
            unset($storeTypes[array_search('AS', $storeTypes)]);
            $showAppleStore = false;
        }

        //Show only Play Market (PM) transactions - default true
        $showPlayMarket = true;
        if (isset($request->showpm) && $request->showpm === 'false') {
            unset($storeTypes[array_search('PM', $storeTypes)]);
            $showPlayMarket = false;
        }

        //order - default by User Purchase Date
        $order = $request->input('order', 'purchase');

        //orderDirection - default DESC
        $orderDirection = $request->input('orderdirection', 'DESC');

        $query = Transaction::with(['user', 'tariff'])
            ->select('transactions.*')
            ->leftJoin('users', 'users.id', '=', 'transactions.user_id');

        $query = $showActive ? $query->notCancelled() : $query;

        $query = $query->hasStoreTypes($storeTypes);

        $query = $query
            ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
            ->where('type', Transaction::TYPE_ORIGINAL);

        $transactionsAmount = $rebillsTotalCount = 0;
        foreach ($query->get() as $transaction) {
            if (isset($transaction->tariff)) {
                $transactionsAmount += $transaction->tariff->price;
                $rebillsTotalCount += $transaction->rebills_count;
            }
        }

        switch ($order) {
            case 'user':
                $query = $query->orderBy('users.created_at', $orderDirection);
                break;
            case 'purchase':
                $query = $query->orderBy('purchase_date', $orderDirection);
                break;
        }

        $transactions = $needPaginate ? $query->paginate() : $query->get();

        return [
            'daterange' => $dateRange,
            'showactive' => $showActive,
            'showas' => $showAppleStore,
            'showpm' => $showPlayMarket,
            'transactions' => $transactions,
            'transactions_amount' => $transactionsAmount,
            'rebills_count' => $rebillsTotalCount,
            'order' => $order,
            'order_direction' => $orderDirection,
        ];
    }

}
