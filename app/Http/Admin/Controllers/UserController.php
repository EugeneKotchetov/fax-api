<?php

namespace App\Http\Admin\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        //Today by default or when request is empty
        $dateRange = [Carbon::now()->format('Y-m-d'), Carbon::now()->format('Y-m-d')];

        if (isset($request->daterange)) {
            $dateRange = json_decode($request->daterange);
        }

        $dateStart = date($dateRange[0] . ' 00:00:00');
        $dateEnd= date($dateRange[1] . ' 23:59:59');

        //Show only paid users - default false
        $showPaid = false;

        if (isset($request->showpaid) && $request->showpaid === 'true') {
            $showPaid = true;
        }

        $users = $showPaid ? User::query()->hasPaid() : User::query();

        $users = $users
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->orderByDesc('id')
            ->paginate();

        $data = [
            'daterange' => $dateRange,
            'showpaid' => $showPaid,
            'users' => $users,
        ];

        return view('users.index', $data);
    }

    /**
     * Toggle is_active status
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggleActive(User $user)
    {
        $user->is_active = !$user->is_active;
        $user->save();

        return back();
    }
}
