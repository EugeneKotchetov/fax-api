<?php

namespace App\Http\Admin\Controllers;

use App\Models\Tariff;
use App\Http\Requests\TariffRequest;

class TariffController extends Controller
{
    /**
     * Display a listing of the admins
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'tariffs' => Tariff::all(),
        ];
        return view('tariffs.index', $data);
    }

    /**
     * Show the form for creating tariff.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('tariffs.create');
    }

    /**
     * Show the form for editing tariff.
     *
     * @param Tariff $tariff
     * @return \Illuminate\View\View
     */
    public function edit(Tariff $tariff)
    {
        $data = [
            'tariff' => $tariff,
        ];
        return view('tariffs.edit', $data);
    }

    /**
     * Store tariff
     *
     * @param TariffRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TariffRequest $request)
    {

        Tariff::create($request->all());

        return redirect()->route('tariffs.index')->withStatus(__('Tariff successfully created.'));
    }

    /**
     * Update admin profile
     *
     * @param TariffRequest $request
     * @param Tariff $tariff
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TariffRequest $request, Tariff $tariff)
    {
        $tariff->update($request->all());

        return redirect()->route('tariffs.index')->withStatus(__('Tariff successfully updated.'));
    }

    /**
     * Destroy admin profile
     *
     * @param Tariff $tariff
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Tariff $tariff)
    {
        $tariff->delete();

        return back()->withStatus(__('Tariff successfully deleted.'));
    }

}
