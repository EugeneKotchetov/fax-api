<?php

namespace App\Http\Admin\Controllers;

use App\Models\Tariff;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Transaction;

class CohortController extends Controller
{
    /** @var Request */
    private $request;

    /** @var int */
    private $koef = 1;

    public function __construct(Request $request)
    {
        $this->request = $request;

        if ($request->input('amount70')) {
            $this->koef = 0.7;
        }

        if ($request->input('amount85')) {
            $this->koef = 0.85;
        }
    }

    /**
     * get View of cohort-statistics page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'months' => $this->getMonths('M, Y'),
            'rows' => $this->getData(),
        ];

        return view('cohort.index', $data);
    }

    /**
     * get array of all transactions months
     *
     * @param string $format
     * @return array
     */
    private function getMonths(string $format): array
    {

        $startDate = '2020-10-01 00:00:00'; //Transaction::query()->min('created_at');
        $endDate = Transaction::query()->max('created_at');
        $months = [];
        $period = CarbonPeriod::create($startDate, '1 month', $endDate);
        foreach ($period as $dt) {
            $months[] = $dt->format($format);
        }

        return $months;
    }

    /**
     * get array of cohort data
     *
     * @return array
     */
    private function getData(): array
    {
        $monthsCol = $this->getMonths('Y-m');
        $monthsRow = $this->getMonths('Y-m');

        $data = [];

        foreach ($monthsRow as $monthRow) {
            $monthRowCarbon = Carbon::parse($monthRow);
            $row = [];
            $row[] = $monthRowCarbon->format('M, Y');
            foreach ($monthsCol as $monthCol) {
                $monthColCarbon = Carbon::parse($monthCol);
                if ($monthColCarbon < $monthRowCarbon) {
                    $row[] = '';
                } elseif ($monthColCarbon == $monthRowCarbon) {
                    $row[] = $this->getTransactions($monthColCarbon);
                } else {
                    $row[] = $this->getRebills($monthRowCarbon, $monthColCarbon);
                }
            }
            $data[] = $row;
        }

        return $data;
    }

    /**
     * get transactions amount for month
     *
     * @param $month
     * @return float
     */
    private function getTransactions($month): string
    {
        $dateStart = $month->firstOfMonth()->format('Y-m-d') . ' 00:00:00';
        $dateEnd = $month->lastOfMonth()->format('Y-m-d') . ' 23:59:59';

        $transactionsAmount = DB::table('transactions')
            ->selectRaw("SUM(tariffs.price) ts_amount, DATE_FORMAT(transactions.created_at, '%Y-%m') name")
            ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
            ->where('transactions.type', Transaction::TYPE_ORIGINAL)
            ->whereBetween('transactions.created_at', [$dateStart, $dateEnd])
            ->groupBy('name')
            ->first();

        $amount = $transactionsAmount ? round($transactionsAmount->ts_amount * $this->koef, 2) : 0;

        if ($this->request->input('rebill')) {
            $amount += $this->getRebills($month, $month);
        }

        return $amount ? '$' . $amount : 0;
    }

    /**
     * get rebills amount for month
     *
     * @param $month
     * @return float
     */
    private function getRebills($monthRow, $monthCol): string
    {
        $dateStart = $monthRow->firstOfMonth()->format('Y-m-d') . ' 00:00:00';
        $dateEnd = $monthRow->lastOfMonth()->format('Y-m-d') . ' 23:59:59';

        $dateMonthCol = $monthCol->format('Y-m');

        $renewedAmounts = DB::table('transactions')
            ->selectRaw("SUM(tariffs.price) rb_amount, DATE_FORMAT(transactions.created_at, '%Y-%m') name")
            ->leftJoin('tariffs', 'tariffs.apple_id', '=', 'transactions.tariff_apple_id')
            ->leftJoin('transactions as tn',  function (JoinClause $join) {
                $join->on('tn.original_transaction_id', '=', 'transactions.original_transaction_id')
                    ->on('tn.type', '=', DB::raw("'".Transaction::TYPE_ORIGINAL."'"));
            })
            ->where('transactions.type', Transaction::TYPE_RE_BILL)
            ->whereBetween('tn.purchase_date', [$dateStart, $dateEnd])
            ->groupBy('name')
            ->get();

        $renewedAmount = $renewedAmounts->where('name', $dateMonthCol)->first();

        return $renewedAmount ? round($renewedAmount->rb_amount * $this->koef, 2) : '0';
    }


}
