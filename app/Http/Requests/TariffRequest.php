<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TariffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'apple_id' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'discount' => 'required|string|max:255',
            'by_default' => 'boolean',
            'price' => 'required',
            'days' => 'required|integer|min:0|max:730',
            'is_active' => 'boolean',
            'sort' => 'required|integer',
            'group' => 'required|integer',
        ];
    }
}
