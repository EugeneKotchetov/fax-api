<?php

namespace App\Services;

class AppleService
{
    /**
     * Connect to Apple and check is it successful
     *
     * @param string $receipt
     * @param bool $isTest
     * @return bool|string
     */
    public function verifyReceipt(string $receipt, $isTest = false)
    {
        if (env('APP_ENV') == 'prod' && $isTest == false) {
            $url = 'https://buy.itunes.apple.com/verifyReceipt'; // for production
        } else {
            $url = 'https://sandbox.itunes.apple.com/verifyReceipt';
        }

        $request = json_encode([
            "receipt-data" => $receipt,
            "password" => env('APPLE_PASSWORD')
        ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }
}