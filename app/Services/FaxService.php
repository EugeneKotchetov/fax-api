<?php

namespace App\Services;

use App\Models\Fax;
use App\Models\FaxFiles;
use ClickSend\Api\FAXApi;
use Illuminate\Support\Facades\Log;
use Twilio\Rest\Client;
use ClickSend\Configuration;

class FaxService
{
    /** @var Client */
    private $twilio;

    /** @var FAXApi  */
    private $clickSend;

    public function __construct()
    {
        $sid    = env('TWILIO_SID');
        $token  = env('TWILIO_TOKEN');

        $config = Configuration::getDefaultConfiguration()
            ->setUsername(env('CLICKSEND_USERNAME'))
            ->setPassword(env('CLICKSEND_PASSWORD'));

        $this->clickSend = new FAXApi(
        // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
        // This is optional, `GuzzleHttp\Client` will be used as default.
            new \GuzzleHttp\Client(),
            $config
        );

        try {
            $this->twilio = new Client($sid, $token);
        } catch (\Exception $e) {
            Log::error('Can\'t create twilio client: ' . $e->getMessage());
        }

    }

    /**
     * Отправка факса через стороннего провайдера
     *
     * @param Fax $fax
     * @return bool
     */
    public function send(Fax $fax)
    {
        try {

            if (count($fax->files) == 0) {
                throw new \Exception('count files = 0');
            }

            $fax->attempt++;

            if ($fax->attempt > 2) {
                throw new \Exception();
            }

            foreach ($fax->files as $file) {
                $file->fax_sid = $this->twilioSend($fax->dst, env('APP_URL') . '/' . $file->file_path);
                $file->status = null;
                $file->save();

                sleep(1);
            }

            $fax->markAsSent();
            $fax->status = null;
            $fax->save();

            return true;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            $fax->status = 'failed';
            $fax->save();

            return false;
        }
    }

    /**
     * Дадим отправлять не более 100 страниц в неделю
     *
     * @param int $userId
     * @param string $dst
     * @return bool
     */
    public function isLimitExceed(int $userId, $dst): bool
    {
        $dateFrom = date('Y-m-d 00:00:00', strtotime('-1 day'));
        $dateTo   = date('Y-m-d 23:59:59');

        $countFiles = FaxFiles::query()
            ->join('faxes', 'fax_files.fax_id', '=', 'faxes.id')
            ->where('faxes.user_id', $userId)
            //->whereIn('status', Fax::$successStatuses)
            ->whereBetween('faxes.created_at', [$dateFrom, $dateTo])
            ->count();

        $countFaxes = Fax::withTrashed()
            ->where('user_id', $userId)
            ->whereBetween('created_at', [$dateFrom, $dateTo])
            ->count();

        $countFaxesWithSameDst = Fax::withTrashed()
            ->where('dst', $dst)
            ->whereBetween('created_at', [$dateFrom, $dateTo])
            ->count();

        Log::error('User ID: ' . $userId . '; Count files: ' . $countFiles. '; count total faxes: ' . $countFaxes);
        if ($countFiles > 100 || $countFaxes > 10 || $countFaxesWithSameDst > 5) {
            return true;
        }

        return false;
    }

    /**
     * Re send some files if busy or no-answer
     *
     * @param FaxFiles $file
     */
    public function resend(FaxFiles $file)
    {
        $file->fax_sid = $this->twilioSend($file->fax->dst, env('APP_URL') . '/' . $file->file_path);
        $file->status = null;
        $file->save();
    }

    /**
     * https://www.twilio.com/docs/fax/send
     * https://www.twilio.com/docs/fax/api/fax-resource#fax-status-values
     *
     * @param string $dst
     * @param string $fileName
     */
    private function twilioSendOld(string $dst, string $fileName)
    {
        $options = [
            'from' => '+' . env('FAX_FROM'), // '+14156589119',
        ];

        $fax = $this->twilio->fax->v1->faxes->create($dst, $fileName, $options);

        //Log::error('twilio: ' . print_r($fax, true));
        return $fax->sid;
    }

    /**
     * https://github.com/ClickSend/clicksend-php/blob/master/docs/Api/FAXApi.md
     * https://developers.clicksend.com/docs/rest/v3/?php#send-fax
     *
     * @param string $dst
     * @param string $fileName
     *
     * @throws \ClickSend\ApiException
     */
    private function twilioSend(string $dst, string $fileName)
    {
        $faxMessage = new \ClickSend\Model\FaxMessage();
        $faxMessage->setTo($dst);
        $faxMessage->setSource('php');
        $faxMessage->setFrom('');

        $faxMessageCollection = new \ClickSend\Model\FaxMessageCollection();
        $faxMessageCollection->setFileUrl($fileName);

        $faxMessageCollection->setMessages([$faxMessage]);

        $response = json_decode($this->clickSend->faxSendPost($faxMessageCollection));
        if (isset($response->data) && isset($response->data->messages)) {
            return $response->data->messages[0]->message_id;
        }
    }

    /**
     * https://github.com/ClickSend/clicksend-php/blob/master/docs/Api/FAXApi.md#faxReceiptsByMessageIdGet
     *
     * Get status by id
     *
     * @param string $sid
     * @return mixed
     * @throws \ClickSend\ApiException
     */
    public function getStatus(string $sid)
    {
        $fax = json_decode($this->clickSend->faxReceiptsByMessageIdGet($sid));
        if (isset($fax->data) && $status = $fax->data->status) {
            return $status;
        }
    }

    /**
     * https://www.twilio.com/docs/fax/api/fax-resource#fetch-a-fax-resource
     * https://www.twilio.com/docs/fax/api/fax-resource#fax-status-values
     *
     * @param string $sid
     * @return string
     */
    public function getStatusOld(string $sid)
    {
        $fax = $this->twilio->fax->v1->faxes($sid)->fetch();

        return $fax->status;
    }
}