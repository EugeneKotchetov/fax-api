<?php

namespace App\Services;


class PushService
{

    public function send(array $tokens)
    {
        $data = [
            'registration_ids' => $tokens,
            'notification' => [
                'title' => 'The Fax App for iPhone',
                'body' => 'Forget something? If you\'d like to send a Fax, start now.',
            ],
        ];

        return $this->fcm($data);
    }

    private function fcm($data)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $key = env('FCM_KEY');
        $fields = json_encode($data);

        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=' . $key,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, TRUE);
    }
}
