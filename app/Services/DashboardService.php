<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Transaction;
use Carbon\CarbonPeriod;

/**
 * Class DashboardService
 *
 */
class DashboardService
{
    /**
     * Возвращает массив дат вида ['01.01', '02.01' ... '14.01']
     *
     * @param array $dateRange
     * @return array
     */
    private function getDateRangeArray(array $dateRange): array
    {
        $dateStart = date($dateRange[0]->format('Y-m-d') . ' 00:00:00');
        $dateEnd= date($dateRange[1]->format('Y-m-d') . ' 23:59:59');
        $period = CarbonPeriod::create($dateStart, $dateEnd);

        $dates = [];
        foreach ($period as $date) {
            $dates[] = $date->format('d.m');
        }

        return [
            'period' => $period,
            'dates' => $dates,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ];
    }

    /**
     * Возвращает массив данных с подстановкой нулей, для дат с пустыми значениями
     *
     * @param array $dateRange
     * @param $items
     * @return array
     */
    private function getDataSetArray(array $dateRange, $items): array
    {
        $dataSet = [];
        foreach ($this->getDateRangeArray($dateRange)['period'] as $date) {
            $item = $items->where('date', date($date->format('Y-m-d')))->first();
            if ($item) {
                $dataSet[] = $item->value;
            } else {
                $dataSet[] = 0;
            }
        }

        return $dataSet;
    }

    /**
     * Возвращает массив данных, формата charts.js data
     *
     * @param string $type
     * @param array $dateRange
     * @param int $total
     * @param array|null $chartSelect
     * @param array $data
     * @return array
     */
    private function getData(array $dateRange, int $total, array $data): array
    {
        return [
            'total' => $total,
            'chat' => [
                'data' => [
                    'labels' => $this->getDateRangeArray($dateRange)['dates'],
                    'datasets' => [
                        [
                            'label' => '',
                            'data' => $data,
                        ],
                    ]
                ]
            ]
        ];
    }

    /**
     * Возвращает массив данных для графика Users в зависимости от диапазона дат $dateRange
     *
     * @param array $dateRange
     * @return array
     */
    public function getUsersChartData(array $dateRange): array
    {
        $query = User::query()
            ->whereBetween('created_at', [$this->getDateRangeArray($dateRange)['dateStart'], $this->getDateRangeArray($dateRange)['dateEnd']]);

        $total = $query->count();

        $users = $query
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get([DB::raw("DATE(created_at) as date"), DB::raw("count(id) as value")]);

        $data = $this->getDataSetArray($dateRange, $users);

        return $this->getData($dateRange, $total, $data);
    }

    /**
     * Возвращает массив данных для графика Transactions в зависимости от диапазона дат $dateRange
     *
     * @param array $dateRange
     * @return array
     */
    public function getTransactionsChartData(array $dateRange): array
    {
        $query = Transaction::query()
            ->where('type', Transaction::TYPE_ORIGINAL)
            ->whereBetween('created_at', [$this->getDateRangeArray($dateRange)['dateStart'], $this->getDateRangeArray($dateRange)['dateEnd']]);

        $total = $query->count();

        $transactions = $query
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get([DB::raw("DATE(created_at) as date"), DB::raw("count(id) as value")]);

        $data = $this->getDataSetArray($dateRange, $transactions);

        return $this->getData($dateRange, $total, $data);
    }

    /**
     * Возвращает данные для Rebills в зависимости от диапазона дат $dateRange
     *
     * @param array $dateRange
     * @return array
     */
    public function getRebillsData(array $dateRange): array
    {
        $query = Transaction::query()
            ->where('type', Transaction::TYPE_RE_BILL)
            ->whereBetween('created_at', [$this->getDateRangeArray($dateRange)['dateStart'], $this->getDateRangeArray($dateRange)['dateEnd']]);

        $total = $query->count();

        return ['total' => $total];
    }

}
