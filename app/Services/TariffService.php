<?php

namespace App\Services;

use App\Models\Tariff;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class TariffService
 *
 */
class TariffService
{
    /**
     * @param User $user
     * @return Collection
     */
    public function getList(User $user): Collection
    {
        $tariffs = Tariff::select(['id', 'title', 'apple_id', 'price', 'description', 'days', 'discount', 'by_default', 'group'])
            ->whereIn('id', json_decode($user->tariff_list))
            ->orderBy('sort')
            ->get();

        foreach ($tariffs as $key => $tariff) {
            $tariff->by_default = (bool)$tariff->by_default;
            $tariff->is_active = $user->tariff_id == $tariff->id && $user->paid_until > time();
            $tariff->paid_until = $user->tariff_id == $tariff->id ? date('Y-m-d H:i:s', $user->paid_until) : null;

            // если есть купленный тариф
            if ($user->tariff_id > 0) {
                $tariff->description = "$" . $tariff->price . '/' . $tariff->description;

                // у выбранного тарифа добавить текст и скидка=0
                if ($user->tariff_id == $tariff->id) {
                    //$tariff->description .= ' - Current plan';
                    $tariff->discount = 0;
                } else if ($user->tariff_id > $tariff->id) {
                    $tariff->discount = 0;
                } else if ($user->tariff_id == 2) {
                    // только в одном случае, если пользователь выбрал средний тариф, пересчитать скидку для третьего
                    $middleTariff = $tariffs[1];

                    // посчитаем цену среднего тарифа за год
                    $middleYearPrice = (round($tariff->days / $middleTariff->days) * $middleTariff->price);

                    // пересчитаем скидку как сравнение цен
                    $tariff->discount = round(abs(($tariff->price / $middleYearPrice) - 1) * 100);
                }

            } else {
                //иначе вывести инфо про триал
                if (Tariff::TRIAL_DAYS > 0) {
                    $tariff->description = "$" . $tariff->price . '/' . $tariff->description . ' after trial period';
                }
            }
        }

        return $tariffs;
    }

    /**
     * @return array
     */
    public function getAuction(): array
    {
        $isTariffExist = Tariff::where('is_active', true)->get()->count() ? true : false;
        if ($isTariffExist) {
            $randomGroup = DB::table('tariffs')
                ->selectRaw('`group`')
                ->where('is_active', true)
                ->groupBy('group')
                ->pluck('group')
                ->random(1)
                ->first();

            return Tariff::where('is_active', true)
                ->where('group', $randomGroup)
                ->orderBy('sort')
                ->pluck('id')
                ->toArray();
        }

        return [];
    }

}
