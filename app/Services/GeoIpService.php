<?php

namespace App\Services;


/**
 * Class GeoIpService
 *
 */
class GeoIpService
{
    /**
     * @return mixed|string
     */
    public function getIp()
    {
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $ip = urldecode($ip);
        if (strstr($ip, ',')) {
            $ips = explode(',', $ip);
            $ip = $ips[0];
        }

        return $ip;
    }

    /**
     * @param string $ip
     * @return array
     */
    public function getGeoByIp(string $ip): array
    {
        try {
            $url = 'http://pro.ip-api.com/json/'.$ip.'?key=cVyJIXGDaLnjd0r';
            $data = file_get_contents($url);

            $object = json_decode($data);

            return [
                'country' => $object->countryCode ?? '',
                'city' => $object->city ?? '',
            ];
        } catch (\Exception $e) {
            return [
                'country' => '',
                'city' => ''
            ];
        }
    }

}
