<?php

namespace App\Integrations;

/**
 * Class MFax
 * https://docs.documo.com/#fax
 *
 * @package App\Integrations
 */
class MFax implements IntegrationInterface
{
    public function send(string $dst, string $fileName)
    {

    }

    public function getStatus(string $faxId)
    {

    }
}