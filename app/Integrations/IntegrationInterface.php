<?php

namespace App\Integrations;

interface IntegrationInterface {

    public function send(string $dst, string $fileName);

    public function getStatus(string $sid);
}